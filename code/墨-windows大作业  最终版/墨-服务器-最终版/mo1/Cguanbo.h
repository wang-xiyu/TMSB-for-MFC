#pragma once


// Cguanbo 对话框

class Cguanbo : public CDialogEx
{
	DECLARE_DYNAMIC(Cguanbo)

public:
	Cguanbo(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~Cguanbo();

// 对话框数据
	enum { IDD = IDD_DIALOG1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CString m_guanbo;
};
