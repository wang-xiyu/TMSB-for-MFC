
// mo1Dlg.cpp : 实现文件
//

#include "stdafx.h"
#include "mo1.h"
#include "mo1Dlg.h"
#include "afxdialogex.h"
#include "Cguanbo.h"
#include <io.h>
#include <windows.h> 
#include "BookDlg.h"
#include "Mg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
extern CMg* mg;
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
// Cmo1Dlg 对话框




Cmo1Dlg::Cmo1Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(Cmo1Dlg::IDD, pParent)
	, oorc(1)
	, oorc1(false)
	, wt4(NULL)
	, everytime(_T(""))
	, kai(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	for(int i=0;i<50;i++)
		zhongdaxia[i].available=1;

	m_tiaomachaxun = _T("");
}

void Cmo1Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_list);
	DDX_Control(pDX, IDC_LIST2, m_list2);
	DDX_Control(pDX, IDC_LIST3, m_list3);
	DDX_Text(pDX, IDC_EDIT2, m_tiaomachaxun);
	DDV_MaxChars(pDX, m_tiaomachaxun, 7);
}

BEGIN_MESSAGE_MAP(Cmo1Dlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_NCHITTEST()
	ON_BN_CLICKED(IDC_BUTTON6, &Cmo1Dlg::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON7, &Cmo1Dlg::OnBnClickedButton7)
	ON_BN_CLICKED(IDC_BUTTON9, &Cmo1Dlg::OnBnClickedButton9)
	ON_BN_CLICKED(IDC_BUTTON8, &Cmo1Dlg::OnBnClickedButton8)
	ON_BN_CLICKED(IDC_BUTTON10, &Cmo1Dlg::OnBnClickedButton10)


	ON_BN_CLICKED(IDC_BUT_search, &Cmo1Dlg::OnClickedButSearch)
	ON_BN_CLICKED(IDC_BUT_modify, &Cmo1Dlg::OnClickedButModify)
	ON_BN_CLICKED(IDC_BUT_del, &Cmo1Dlg::OnClickedButDel)
	ON_BN_CLICKED(IDC_BUT_add, &Cmo1Dlg::OnClickedButAdd)
	ON_BN_CLICKED(IDC_BUT_all, &Cmo1Dlg::OnClickedButAll)
	ON_BN_CLICKED(IDC_BUT_suit, &Cmo1Dlg::OnClickedButSuit)


	ON_BN_CLICKED(IDC_BUT_seedata, &Cmo1Dlg::OnClickedButSeedata)
END_MESSAGE_MAP()


// Cmo1Dlg 消息处理程序

BOOL Cmo1Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	
	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	count=0;
	m_list.InsertColumn(0,"       类型                 事件                              时间                                  备注");
	m_list.SetColumnWidth(0,875);

	m_list2.InsertColumn(0,"       序列                   IP                              接入时间                                  其他备注");
	m_list2.SetColumnWidth(0,875);
	m_list.ShowWindow(SW_HIDE);
	m_list2.ShowWindow(SW_HIDE);

	m_list3.SetColumnWidth(0,200);
	m_list3.SetColumnWidth(1,200);
	m_list3.SetColumnWidth(2,200);


	GetDlgItem(IDC_BUTTON9)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUT_seedata)->SetWindowText("关闭数据库");
	CString strSource=_T("select mybarcode.barcode,info,gooddate\
						 from mybarcode"); //
	mg->OpenRecordset(strSource);
	CListCtrl* pListCtrl=(CListCtrl*)GetDlgItem(IDC_LIST3);
	mg->m_pListCtrl=pListCtrl;
	mg->AutoOpen();
	
	
	pListCtrl1=(CListCtrl*)GetDlgItem(IDC_LIST3);   //初始化列表框一个指针传到线程111
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void Cmo1Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
	/*
	CDC* pdc=GetDC();                                 //用桌面背景填充对话框111
	::PaintDesktop(pdc->m_hDC);
	ReleaseDC(pdc);
	*/
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR Cmo1Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



LRESULT Cmo1Dlg::OnNcHitTest(CPoint point)
{
	// TODO: Add your message handler code here and/or call default                   //实现客户区移动对话框111
	UINT nValue =CDialog::OnNcHitTest(point);
	if(nValue==HTCLIENT)
	{
		nValue=HTCAPTION;
	}
	return nValue;
	return CDialogEx::OnNcHitTest(point);
}
//服务器线程
UINT thread(LPVOID p)
{
	char buff[11100];
	CSize size;
	size.cx=0;
	size.cy=30;
	int s=1,msgcount,loop=1,flag=0;
	Cmo1Dlg *dlg=(Cmo1Dlg*)AfxGetApp()->GetMainWnd();
	//获得客户端数量
	msgcount=dlg->getcount();
	//关闭服务器111
	
	//关闭服务器111
	msgcount=dlg->getcount();
	if (msgcount==-1)
		loop=0;
	if(loop)    //客户不空    
	{
		s=1;
		
		dlg->msgsock[msgcount]=accept(dlg->sock,(sockaddr*)&(dlg->serv),&(dlg->addlen));    //接受客户   第一次只是建立服务器   建立服务器只在这一步   //||用户申请连接2
		
		if((dlg->msgsock[msgcount]==INVALID_SOCKET)||dlg->oorc1)
		{
			//AfxMessageBox("连接错误");    //关闭后服务器后再申请连接  1
		}
		else
		{    //||用户申请连接1
			CString d;
			CSize size;
			size.cx=0;
			size.cy=30;
			dlg->alltime();
			//m_list.InsertItem(count++,"###系统###    广播已发出           "+everytime+"        "+buf1);    //333
			//m_list.Scroll(size);


			//启动线程
			CString ipadd;
			ipadd=inet_ntoa(dlg->serv.sin_addr);
			AfxBeginThread(thread,0);
			dlg->SetForegroundWindow();
			for(int i=0;i<50;i++)                          //用户数据记录    录入
			{
				if(dlg->zhongdaxia[i].available==1)
				{
					dlg->zhongdaxia[i].IP=ipadd;
					dlg->zhongdaxia[i].Time=dlg->everytime;
					dlg->zhongdaxia[i].Other="空";
					dlg->zhongdaxia[i].available=2;
					break;		
				}	
			
			}
			//dlg->m_list.InsertItem(dlg->count++,)
			dlg->m_list.InsertItem(dlg->count++,"###连接###    连接成功              "+dlg->everytime+"        IP:"+ipadd);

			
			dlg->m_list.Scroll(size);
			while(s!=SOCKET_ERROR)
			{
				//循环接收数据
	
				s=recv(dlg->msgsock[msgcount],buff,11100,0);
				
				
				d=buff;
				
				AfxMessageBox(d);
				
				/*
				dlg->SetForegroundWindow();

				if (s!=SOCKET_ERROR)       //后的接受客户的数据  并显示   111
				{
					dlg->m_list.InsertItem(dlg->count++,"###消息###    匹对:"+ d +"       "+dlg->everytime+"        IP:"+ipadd);
					//dlg->m_list.InsertItem(dlg->count++,buff);
					dlg->m_list.Scroll(size);
					//dlg->sendtoall(dlg->msgsock[msgcount],buff);   //发送群体消息     222
					
					


					//CListCtrl* pListCtrl1=(CListCtrl*)GetDlgItem(IDC_LIST3);         //匹对数据库dlg->m_list3
					
					
					
					
					
					
					
					
					
					
					
		
					int k=dlg->pListCtrl1->GetItemCount();//获得数据库行数总数量
					int panduan=0;//判断是否找到
					for(int i=0;i<k;i++) //匹对数据库  
					{
						if(panduan==0)
							panduan=2;
						CString str1,str2;
						str1=buff;
						str2=dlg->pListCtrl1->GetItemText(i,0);
						if(str1==str2)  //假如匹对成功
						{
							 panduan=1;//找到了
							send(dlg->msgsock[msgcount],dlg->pListCtrl1->GetItemText(i,1),100,0);//返回给查询改条码的用户				   
							break;
						}
						//AfxMessageBox(dlg->pListCtrl1->GetItemText(i,1));						
					}	
					if(panduan==2)
					{
						send(dlg->msgsock[msgcount],"::>_<:: 居然没有找到",100,0);
					}
			
				
					
					
					
					
					
					
					
					
					
					
					//AfxMessageBox("居然没有找到");
					
					/*
					int k=dlg->m_list3.GetItemCount();//获得行数总数量
					CString sh;
					sh.Format("%d",sh);
					AfxMessageBox(k);
					for(int i=0;i<k;i++)
					{
						//AfxMessageBox(dlg.m_barcode);
						//AfxMessageBox(pListCtrl->GetItemText(i,0));
						if(strcmp(buff,dlg->m_list3.GetItemText(i,0)));
						AfxMessageBox(dlg->m_list3.GetItemText(i,1));
						return 0;
					}
					


                 	if(!strcmp(buff,"123"))
					send(dlg->msgsock[msgcount],"出错了！::>_<:: 未找到匹配信息",100,0);   //数据的即时返回         222    待添加 钟大侠
					else
						send(dlg->msgsock[msgcount],"找到了！`(*∩_∩*) ",100,0);
						
				}*/
				
			}
		
			send(dlg->msgsock[msgcount],"成功退出",100,0);     //客户端显示
			dlg->m_list.InsertItem(dlg->count++,"###连接###    用户退出              "+dlg->everytime+"        IP:"+ipadd);     //服务器显示   

			
			for(int i=0;i<50;i++)                          //用户数据记录
			{
				if(dlg->zhongdaxia[i].IP==ipadd)
				{
					dlg->zhongdaxia[i].available=1;     //退房后   标志可住；
				}
			}
			
			dlg->m_list.Scroll(size);
			dlg->msgsock[msgcount]=NULL;
			for (int i=0;i<50;i++)
				if (dlg->msgsock[i]!=NULL)
				{
					flag=1;
				}
			closesocket(dlg->msgsock[msgcount]);
		}
	}
	//终止线程

	AfxEndThread(0);       //结束
	
	return 0;
}

Cmo1Dlg::~Cmo1Dlg()
{
	for (int i=0;i<50;i++)
		if (msgsock[i]!=NULL)
			send(msgsock[i],"服务器已退出",100,0);
}
int Cmo1Dlg::getcount(void)    //获得还没有使用的socket数组号   111
{
	for (int i=0;i<50;i++)
	{
		if (msgsock[i]==NULL)
			return i;
	}
	return -1;
}


void Cmo1Dlg::sendtoall(SOCKET s, char * buff)   //向所有客户发送数据111
{
	int j=0;
	for (int i=0;i<50;i++)
	{
		if (msgsock[i]!=NULL && msgsock[i]!=s)			//发送
		{
			send(msgsock[i],buff,100,0);
			j++;
		}
	}
	if(j==0)
	{
		AfxMessageBox("无活动用户连接,发送失败");
	}
	else
	{
		CString x;
		x.Format("%d",j);
		AfxMessageBox("广播消息已发送---"+x+"个活动用户已接受到信息");
		kai=1;
	}
}



void Cmo1Dlg::OnBnClickedButton6()     //启动服务器   111
{
	// TODO: 在此添加控件通知处理程序代码	

	CSize size;
	size.cx=0;
	size.cy=30;


	GetDlgItem(IDC_BUT_add)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUT_all)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUT_del)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUT_modify)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUT_suit)->EnableWindow(FALSE);



	m_list3.ShowWindow(SW_HIDE);
	m_list2.ShowWindow(SW_HIDE);
	m_list.ShowWindow(SW_SHOW);
	GetDlgItem(IDC_BUT_seedata)->SetWindowText("查看数据库");
	GetDlgItem(IDC_BUTTON9)->EnableWindow(TRUE);
	
	
	if(oorc==-1)          //按下终止服务器  111
	{
		oorc=1;
		oorc1=true;
		for (int i=0;i<50;i++)
			if (msgsock[i]!=NULL)
				send(msgsock[i],"服务器已退出",100,0);
		GetDlgItem(IDC_BUTTON6)->SetWindowText("开启||实时监听");	
		if(oorc1)
		{
			int msgcount;
			msgcount=getcount();
			msgsock[msgcount]=NULL;
			
			alltime();
			m_list.InsertItem(count++,"###系统###    服务器已关闭        "+everytime);   
			m_list.Scroll(size);


			msgsock[msgcount]=NULL;
		    int flag=0;
			for (int i=0;i<50;i++)
			{
				closesocket(msgsock[i]);
				if (msgsock[i]!=NULL)
					flag=1;
			}
			closesocket(msgsock[msgcount]);
			//ExitThread(10);	
			//return (DWORD)tbi.ClientId.UniqueThread; //返回线程ID   
			//GetThreadId(wt4->m_hThread);
			WSACleanup( );

			::PostThreadMessage(GetThreadId(wt4->m_hThread),WM_QUIT,0,0);    //关闭线程
			AfxMessageBox("服务器已关闭");
			// ::CloseHandle(wt4->m_hThread);
			//AfxEndThread(0);return ;
		}
		return;
	}


	if(oorc==1)     //启动服务器   111
	{
		oorc=-1;
		oorc1=false;
		GetDlgItem(IDC_BUTTON6)->SetWindowText("停止||关闭服务");
		//初始化socket库      111
		for (int i=0;i<50;i++)
		msgsock[i]=NULL;

	    WORD wVersionRequested;
	    WSADATA wsaData;
	    int err;
	    wVersionRequested = MAKEWORD( 1, 1 );
	    err = WSAStartup( wVersionRequested, &wsaData );
	    if ( err != 0 ) 
	    {
		    MessageBox("错误");
	    }
	    if ( LOBYTE( wsaData.wVersion ) != 1 ||
		     HIBYTE( wsaData.wVersion ) != 1 ) 
	    {
		     WSACleanup( );
	    }
	//初始化套接字     111
	    serv.sin_addr.s_addr=htonl(INADDR_ANY);
	    serv.sin_family=AF_INET;
	    serv.sin_port=5000;//端口  5000
	    addlen=sizeof(serv);
	    sock=socket(AF_INET,SOCK_STREAM,0);    //创建socket
	    if (bind(sock,(sockaddr*)&serv,addlen))
	    {
		   AfxMessageBox("绑定错误");
	    }
	    else
	    {
		//m_list.InsertItem(count++,inet_ntoa(serv.sin_addr));
		   
		   alltime();
		   m_list.InsertItem(count++,"###系统###    服务器已创建        "+everytime);    //333
		   m_list.Scroll(size);

		   AfxMessageBox("服务器已创建");

		//开始侦听 
		   listen(sock,10);
		//调用线程
		   wt4 =AfxBeginThread(thread,NULL);    //线程ID
	    }
	}
	
}


void Cmo1Dlg::OnBnClickedButton7()             // 广播
{
	// TODO: 在此添加控件通知处理程序代码
	CString buf;
	int msgcount;
	Cmo1Dlg *dlg=(Cmo1Dlg*)AfxGetApp()->GetMainWnd();
		msgcount=dlg->getcount();
	    if(oorc1||(oorc==1))
		{
			AfxMessageBox("错误：未检测到活动连接");
			return;
		}
	Cguanbo dlg1;
	if(dlg1.DoModal()==IDOK)
	{
		buf=dlg1.m_guanbo;
	}
	char buf1[100];
	strcpy(buf1,buf);
	
	dlg->sendtoall(dlg->msgsock[msgcount],buf1); //   修改部分   222

	if(kai)
	{
		CSize size;
		size.cx=0;
		size.cy=30;
		alltime();
		m_list.InsertItem(count++,"###系统###    广播已发出           "+everytime+"        "+buf1);    //333
		m_list.Scroll(size);
	}



}


int Cmo1Dlg::alltime(void)    //获得时间
{
	SYSTEMTIME sys; 
	GetLocalTime( &sys ); 
	everytime.Format("%d/%d/%d-%d:%d:%d-星期",sys.wYear,sys.wMonth,sys.wDay 
		,sys.wHour,sys.wMinute,sys.wSecond);
	switch(sys.wDayOfWeek)
	{
	case 1:
		everytime=everytime+"一";
		break;
	case 2:
		everytime=everytime+"二";
		break;
	case 3:
		everytime=everytime+"三";
		break;
	case 4:
		everytime=everytime+"四";
		break;
	case 5:
		everytime=everytime+"五";
		break;
	case 6:
		everytime=everytime+"六";
		break;
	case 0:
		everytime=everytime+"日";
		break;

	}
	return 0;
}


void Cmo1Dlg::OnBnClickedButton9()     //清屏  
{
	// TODO: 在此添加控件通知处理程序代码
	
	m_list.DeleteAllItems();
}


void Cmo1Dlg::OnBnClickedButton8()     //查看所有用户连接
{
	// TODO: 在此添加控件通知处理程序代码
	m_list3.ShowWindow(SW_HIDE);
	m_list2.DeleteAllItems();
	CString str;
	GetDlgItem(IDC_BUTTON8)->GetWindowText(str);
	if(str=="查看网络信息")
	{
		GetDlgItem(IDC_BUTTON9)->EnableWindow(TRUE);
		//GetDlgItem(IDC_BUTTON9)->EnableWindow(FALSE);
		m_list.ShowWindow(SW_SHOW);
		m_list2.ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BUT_seedata)->SetWindowText("查看数据库");
		GetDlgItem(IDC_BUTTON8)->SetWindowText("查看当前用户连接");
	}
	else
	{
		
		//GetDlgItem(IDC_BUTTON9)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUTTON9)->EnableWindow(FALSE);
		m_list.ShowWindow(SW_HIDE);
	    m_list2.ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BUT_seedata)->SetWindowText("查看数据库");
	    GetDlgItem(IDC_BUTTON8)->SetWindowText("查看网络信息");
		CSize size;
		size.cx=0;
		size.cy=30;
		count=0;
		for(int i=0;i<50;i++)
		{
			if(zhongdaxia[i].available==2)
			{
			CString str;
			str.Format("%d",i+1);
			m_list2.InsertItem(count++,str+"                   "+zhongdaxia->IP +"                   "+zhongdaxia->Time+"                   "+zhongdaxia->Other);    //333
			m_list2.Scroll(size);	
			}
		}
	}
}


void Cmo1Dlg::OnBnClickedButton10()   ///保存记录
{
	// TODO: 在此添加控件通知处理程序代码
	CStdioFile F;
 CString str;
 CString str1 = "C:\\caozuojilu.txt";
 F.Open(str1,CFile::modeCreate | CFile::modeWrite);
 F.Write(everytime,everytime.GetLength());
 str1="\n";
 F.Write(str1,str1.GetLength());
 for(int i=0;i<1000;i++)
 {

 
 CString buffer="";
 buffer+=m_list.GetItemText(i,0);
 buffer+="\n";
 F.Write(buffer,buffer.GetLength());
 }
 AfxMessageBox("文件已保存--->C:\\caozuojilu.txt");
 F.Close();
}



void Cmo1Dlg::OnClickedButSearch()
{
	
	/*
	CString strname;
	GetDlgItemText(IDC_EDIT2,strname);
	if(strname=="")
	{
	  AfxMessageBox(_T("~\(≧▽≦)/~请输入条形码"));  
	  return;
	}

	CString strSource=_T("");
	strSource.Format(_T("SELECT * FROM mybarcode where barcode=%s"),strname);
	
	mg->OpenRecordset(strSource);
	if(mg->IsEmpty())
	{
		AfxMessageBox(_T("请输入正确的条码！"));
		return;
	}
	CListCtrl* pListCtrl=(CListCtrl*)GetDlgItem(IDC_LIST3);
	mg->m_pListCtrl=pListCtrl;
	mg->AutoOpen();
	*/
	AfxMessageBox("为中华之崛起而做软件\n                   ------墨");
	// TODO: 在此添加控件通知处理程序代码
}


void Cmo1Dlg::OnClickedButModify()
{
	
	CListCtrl* pListCtrl=(CListCtrl*)GetDlgItem(IDC_LIST3);
	POSITION pos=pListCtrl ->GetFirstSelectedItemPosition();
	if(pos==NULL)
	{
		AfxMessageBox(_T("请先选中记录"));
		return;
	}
	int nItem=pListCtrl->GetNextSelectedItem(pos);
	//CString strBook=pListCtrl->GetItemText(nItem,0);
	CBookDlg dlg;
	
		dlg.m_barcode=pListCtrl->GetItemText(nItem,0);
		dlg.m_info=pListCtrl->GetItemText(nItem,1);
		dlg.m_date=pListCtrl->GetItemText(nItem,2);
	  
	
	if(dlg.DoModal()==IDOK)
	{
		if(dlg.m_barcode.GetLength()<7)
		{
			AfxMessageBox("出错了::>_<:: 条码不正确");
			return;
		}
		if(dlg.m_info=="")
		{
			AfxMessageBox("出错了::>_<:: 背景资料怎么能为空");
			return;
		}


		int k=pListCtrl->GetItemCount();//获得行数总数量
		for(int i=0;i<k;i++)
		{
			//AfxMessageBox(dlg.m_barcode);
			//AfxMessageBox(pListCtrl->GetItemText(i,0));
			if(dlg.m_barcode==pListCtrl->GetItemText(i,0))
			{
				AfxMessageBox("修改失败::>_<::因为已经存在了 ");
				return;
			}
			
		}




		 CString strSource=_T("");
		 
		 strSource.Format((_T("update  mybarcode set barcode=%s,info='%s',gooddate='%s'\
							  where barcode=%s")),dlg.m_barcode,dlg.m_info,dlg.m_date,pListCtrl->GetItemText(nItem,0));
		 
		 if(dlg.m_barcode==pListCtrl->GetItemText(nItem,0)&&dlg.m_info==pListCtrl->GetItemText(nItem,1))
		 {
			 AfxMessageBox("╰_╯ 被骗了。根本没变化");
			 return;
		 }
		 

		 if(mg->Excute(strSource))
		 {

			 AfxMessageBox(_T("    ^_^\n修改成功！"));
			 CString strSource=_T("");
			 CString Source=_T("select mybarcode.barcode,info,gooddate \
							   from mybarcode"); 
			 mg->OpenRecordset(Source);
			 CListCtrl* pListCtrl=(CListCtrl*)GetDlgItem(IDC_LIST3);
			 mg->m_pListCtrl=pListCtrl;
			 mg->AutoOpen();
		 }
		 else
			 AfxMessageBox(_T("请填入正确的商家信息！"));
	}		
	

}


void Cmo1Dlg::OnClickedButDel()
{

	CListCtrl* pListCtrl=(CListCtrl*)GetDlgItem(IDC_LIST3);
	POSITION pos=pListCtrl ->GetFirstSelectedItemPosition();
	if(pos==NULL)
	{
		AfxMessageBox(_T("请先选中记录"));
		return;
	}
	if(AfxMessageBox(_T("确定删除？"),MB_YESNO)!=IDYES)
		return;
	int nItem=pListCtrl->GetNextSelectedItem(pos);
	CString strBook=pListCtrl->GetItemText(nItem,0);
	CString strSource=_T("");
	strSource.Format(_T("delete from mybarcode where barcode=%s"),strBook);
	if(mg->Excute(strSource))
		AfxMessageBox(_T("    ^_^\n删除成功！"));
	CString Source=_T("select mybarcode.barcode,info,gooddate \
					  from mybarcode"); 
	mg->OpenRecordset(Source);
	CListCtrl* tListCtrl=(CListCtrl*)GetDlgItem(IDC_LIST3);
	mg->m_pListCtrl=tListCtrl;
	mg->AutoOpen();
}


void Cmo1Dlg::OnClickedButAdd()   //数据库添加
{
	CBookDlg dlg;
	alltime();//获得时间
	if(dlg.DoModal()==IDOK)
	{
		CString strSource=_T("");
		if(dlg.m_barcode.GetLength()<7)
		{
			AfxMessageBox("出错了::>_<:: 条码不正确");
			return;
		}
		if(dlg.m_info=="")
		{
			AfxMessageBox("出错了::>_<:: 背景资料怎么能为空");
			return;
		}
	

		CListCtrl* pListCtrl=(CListCtrl*)GetDlgItem(IDC_LIST3);
		int k=pListCtrl->GetItemCount();//获得行数总数量
		for(int i=0;i<k;i++)
		{
			if(dlg.m_barcode==pListCtrl->GetItemText(i,0))
			{
				
				AfxMessageBox("无法添加::>_<::因为已经存在了 ");
				return;
			}		
		}





		strSource.Format(_T("INSERT INTO mybarcode values (%s,'%s','%s')"),dlg.m_barcode,dlg.m_info,everytime);
		if(mg->Excute(strSource))
		{
			AfxMessageBox(_T("    ^_^\n添加成功！"));
			CString strSource=_T("");
			strSource.Format(_T("SELECT * FROM mybarcode where info='%s'"),dlg.m_info);
			/*mg->OpenRecordset(strSource);
			CListCtrl* pListCtrl=(CListCtrl*)GetDlgItem(IDC_LIST2);
			mg->m_pListCtrl=pListCtrl;
			mg->AutoOpen();*/

			CString Source=_T("select mybarcode.barcode,info,gooddate \
						 from mybarcode"); 
	       mg->OpenRecordset(Source);
	       CListCtrl* pListCtrl=(CListCtrl*)GetDlgItem(IDC_LIST3);
	       mg->m_pListCtrl=pListCtrl;
	       mg->AutoOpen();
		}
		else
		{
			AfxMessageBox(_T("请输入正确的条码信息！"));
		}
	}		
	// TODO: 在此添加控件通知处理程序代码
}


void Cmo1Dlg::OnClickedButAll()
{
		CString Source=_T("select mybarcode.barcode,info,gooddate \
						 from mybarcode"); /*left outer join 教职工  on 借书单.借书证号=教职工.借书证号*/
	       mg->OpenRecordset(Source);
	       CListCtrl* pListCtrl=(CListCtrl*)GetDlgItem(IDC_LIST3);
	       mg->m_pListCtrl=pListCtrl;
	       mg->AutoOpen();
	
	// TODO: 在此添加控件通知处理程序代码
}




void Cmo1Dlg::OnClickedButSuit()
{
	CString strname;
	GetDlgItemText(IDC_EDIT2,strname);
	if(strname=="")
	{
	  AfxMessageBox(_T("~\(≧▽≦)/~请输入条形码"));  
	  return;
	}
	CString strSource=_T("");
	strSource.Format(_T("SELECT * FROM mybarcode where barcode=%s"),strname);
	
	mg->OpenRecordset(strSource);
	if(mg->IsEmpty())
	{
		AfxMessageBox(_T("找不到-_-|||"));
		return;
	}
	else
	{
		
	CListCtrl* pListCtrl=(CListCtrl*)GetDlgItem(IDC_LIST3);
	mg->m_pListCtrl=pListCtrl;
	mg->AutoOpen();

	POSITION pos=pListCtrl ->GetFirstSelectedItemPosition();
	
	 CString str,s1,s2,s3;
	 s1=pListCtrl->GetItemText(0,0);
	 s2=pListCtrl->GetItemText(0,1);
	 s3=pListCtrl->GetItemText(0,2);
	 str="^_^找到了\n";
		str +="条码:"+s1+"\n"+"商家信息:"+s2+"\n"+"生产日期:"+s3;
	 MessageBox(str);




	
	
}
}


void Cmo1Dlg::OnClickedButSeedata()          //查看数据库  关闭后到网络信息
{
	//m_list.DeleteAllItems();
	//m_list2.DeleteAllItems();
	//m_list.ShowWindow(SW_HIDE);
	//m_list2.ShowWindow(SW_HIDE);
		
	CString str;
	GetDlgItem(IDC_BUT_seedata)->GetWindowText(str);
	if(str=="查看数据库")
	{
		GetDlgItem(IDC_BUT_add)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUT_all)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUT_del)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUT_modify)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUT_suit)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUTTON9)->EnableWindow(FALSE);
		m_list.ShowWindow(SW_HIDE);
		m_list2.ShowWindow(SW_HIDE);
		m_list3.ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BUT_seedata)->SetWindowText("关闭数据库");
		//GetDlgItem(IDC_BUTTON8)->SetWindowText("查看当前用户连接");
	}
	else 
	{

		GetDlgItem(IDC_BUT_add)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUT_all)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUT_del)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUT_modify)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUT_suit)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON9)->EnableWindow(TRUE);
		m_list.ShowWindow(SW_SHOW);
		m_list2.ShowWindow(SW_HIDE);
		m_list3.ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BUT_seedata)->SetWindowText("查看数据库");
	}
	// TODO: 在此添加控件通知处理程序代码
}


BOOL Cmo1Dlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 在此添加专用代码和/或调用基类
	if(pMsg->message==WM_KEYDOWN)
		switch(pMsg->wParam)
	{
		case VK_ESCAPE:
			SendMessage(WM_SYSCOMMAND,SC_MINIMIZE,0);	 
			return TRUE;
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}
