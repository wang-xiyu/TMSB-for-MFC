//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by mo1.rc
//
#define IDD_MO1_DIALOG                  102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     129
#define IDD_DIALOG2                     130
#define IDC_BUTTON1                     1000
#define IDC_BUT_suit                    1000
#define IDC_BUTTON2                     1001
#define IDC_BUT_search                  1001
#define IDC_BUTTON3                     1002
#define IDC_BUT_add                     1002
#define IDC_BUTTON4                     1003
#define IDC_BUT_del                     1003
#define IDC_BUTTON5                     1004
#define IDC_BUT_modify                  1004
#define IDC_BUTTON6                     1005
#define IDC_BUTTON7                     1006
#define IDC_LIST1                       1007
#define IDC_EDIT1                       1008
#define IDC_BUTTON8                     1008
#define IDC_BUTTON9                     1009
#define IDC_EDIT3                       1009
#define IDC_BUTTON10                    1010
#define IDC_LIST2                       1011
#define IDC_BUT_all                     1012
#define IDC_EDIT2                       1013
#define IDC_LIST3                       1014
#define IDC_BUTTON11                    1015
#define IDC_BUT_seedata                 1015

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1016
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
