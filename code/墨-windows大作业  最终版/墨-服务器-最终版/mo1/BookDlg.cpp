// BookDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "mo1.h"
#include "BookDlg.h"
#include "afxdialogex.h"


// CBookDlg 对话框

IMPLEMENT_DYNAMIC(CBookDlg, CDialog)

CBookDlg::CBookDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CBookDlg::IDD, pParent)
{

	m_barcode = _T("");
	m_info = _T("");
	m_date = _T("");
}

CBookDlg::~CBookDlg()
{
}

void CBookDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_barcode);
	DDV_MaxChars(pDX, m_barcode, 7);   //限制最大输入7位数字
	DDX_Text(pDX, IDC_EDIT2, m_info);
	DDX_Text(pDX, IDC_EDIT3, m_date);
}


BEGIN_MESSAGE_MAP(CBookDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CBookDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CBookDlg 消息处理程序


void CBookDlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialog::OnOK();
}


BOOL CBookDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 在此添加专用代码和/或调用基类
	
	return CDialog::PreTranslateMessage(pMsg);
}
