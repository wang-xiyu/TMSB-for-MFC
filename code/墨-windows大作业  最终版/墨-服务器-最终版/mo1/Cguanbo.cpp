// Cguanbo.cpp : 实现文件
//

#include "stdafx.h"
#include "mo1.h"
#include "Cguanbo.h"
#include "afxdialogex.h"


// Cguanbo 对话框

IMPLEMENT_DYNAMIC(Cguanbo, CDialogEx)

Cguanbo::Cguanbo(CWnd* pParent /*=NULL*/)
	: CDialogEx(Cguanbo::IDD, pParent)
{

	m_guanbo = _T("");
}

Cguanbo::~Cguanbo()
{
}

void Cguanbo::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_guanbo);
}


BEGIN_MESSAGE_MAP(Cguanbo, CDialogEx)
END_MESSAGE_MAP()


// Cguanbo 消息处理程序
