// Cipset.cpp : 实现文件
//

#include "stdafx.h"
#include "mo1.h"
#include "Cipset.h"
#include "afxdialogex.h"


// Cipset 对话框

IMPLEMENT_DYNAMIC(Cipset, CDialogEx)

Cipset::Cipset(CWnd* pParent /*=NULL*/)
	: CDialogEx(Cipset::IDD, pParent)
{

	m_ipadd = _T("192.168.1.100");
	m_port = _T("5000");
}

Cipset::~Cipset()
{
}

void Cipset::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_ipadd);
	DDX_Text(pDX, IDC_EDIT2, m_port);
}


BEGIN_MESSAGE_MAP(Cipset, CDialogEx)
END_MESSAGE_MAP()


// Cipset 消息处理程序
