//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by mo1.rc
//
#define IDD_MO1_DIALOG                  102
#define IDR_MAINFRAME                   128
#define IDB_BITMAP1                     129
#define IDB_BITMAP2                     130
#define IDB_BITMAP3                     131
#define IDD_DIALOG1                     132
#define IDB_BITMAP4                     133
#define IDB_BITMAP5                     134
#define IDB_BITMAP6                     135
#define IDB_BITMAP7                     136
#define IDB_BITMAP8                     138
#define IDB_BITMAP9                     141
#define IDD_DIALOG2                     142
#define IDD_DIALOG3                     143
#define IDB_BITMAP10                    146
#define IDB_BITMAP11                    147
#define IDB_BITMAP12                    148
#define IDB_BITMAP13                    149
#define IDB_BITMAP14                    150
#define IDB_BITMAP15                    151
#define IDC_BUTTON1                     1000
#define IDC_BUTTON2                     1001
#define IDC_BUTTON3                     1002
#define IDC_BUTTON4                     1003
#define IDC_BUTTON5                     1004
#define IDC_error                       1005
#define IDC_wait                        1006
#define IDC_EDIT1                       1007
#define IDC_EDIT2                       1008
#define IDC_LIST1                       1008
#define IDC_Screen                      1010
#define IDC_CHECK1                      1011
#define IDC_EDIT_Rate                   1012

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        152
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1013
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
