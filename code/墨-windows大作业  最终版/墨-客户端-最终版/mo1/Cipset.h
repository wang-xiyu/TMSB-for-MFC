#pragma once


// Cipset 对话框

class Cipset : public CDialogEx
{
	DECLARE_DYNAMIC(Cipset)

public:
	Cipset(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~Cipset();

// 对话框数据
	enum { IDD = IDD_DIALOG2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CString m_ipadd;
	CString m_port;
};
