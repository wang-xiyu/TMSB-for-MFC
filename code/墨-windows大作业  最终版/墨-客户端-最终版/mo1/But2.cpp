// But2.cpp : 实现文件
//

#include "stdafx.h"
#include "mo1.h"
#include "But2.h"
#include "mo1Dlg.h"

// CBut2

IMPLEMENT_DYNAMIC(CBut2, CStatic)

CBut2::CBut2()
: m_bOverControl(false)
{

}

CBut2::~CBut2()
{
}


BEGIN_MESSAGE_MAP(CBut2, CStatic)
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()



// CBut2 消息处理程序




void CBut2::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	CStatic::OnMouseMove(nFlags, point);SetCapture();





	//如果鼠标还在窗口内
	if (m_bOverControl)
	{
		Cmo1Dlg * dlg = (Cmo1Dlg*)AfxGetApp()->GetMainWnd();
		CBitmap Bitmap;
		Bitmap.LoadBitmap(IDB_BITMAP12);
		HBITMAP hBitmap = (HBITMAP)Bitmap.Detach();
		//CButton * pButton = (CButton*)GetDlgItem(IDOK);
		//pButton->SetBitmap(hBitmap);
		dlg->but12->SetBitmap(hBitmap);



		CRect rect;
		GetClientRect(rect);
		//鼠标位置离开了客户区,解除鼠标捕获，并改写状态量m_bOverControl
		if (!rect.PtInRect(point))
		{
			m_bOverControl = FALSE;


			Cmo1Dlg * dlg = (Cmo1Dlg*)AfxGetApp()->GetMainWnd();
			CBitmap Bitmap;
			Bitmap.LoadBitmap(IDB_BITMAP7);
			HBITMAP hBitmap = (HBITMAP)Bitmap.Detach();
			//CButton * pButton = (CButton*)GetDlgItem(IDOK);
			//pButton->SetBitmap(hBitmap);
			dlg->but12->SetBitmap(hBitmap);





			ReleaseCapture();

			//......
			//其他鼠标离开时的操作
			//eg: ReDrawWindow();

			return;
		}
	}
	else
	{
		m_bOverControl = TRUE;

		//
		//其他鼠标进入时的操作
		//eg: ReDrawWindow();

		//SetCapture();
	}








}
