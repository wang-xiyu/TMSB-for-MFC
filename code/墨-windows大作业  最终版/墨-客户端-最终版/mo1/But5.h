#pragma once


// CBut5

class CBut5 : public CStatic
{
	DECLARE_DYNAMIC(CBut5)

public:
	CBut5();
	virtual ~CBut5();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	bool m_bOverControl;
};


