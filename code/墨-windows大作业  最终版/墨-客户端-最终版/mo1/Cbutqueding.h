#pragma once


// Cbutqueding

class Cbutqueding : public CStatic
{
	DECLARE_DYNAMIC(Cbutqueding)

public:
	Cbutqueding();
	virtual ~Cbutqueding();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnMouseLeave();
	BOOL m_bOverControl;
};


