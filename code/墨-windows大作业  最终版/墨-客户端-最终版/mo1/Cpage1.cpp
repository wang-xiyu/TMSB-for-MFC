// Cpage1.cpp : 实现文件
//

#include "stdafx.h"
#include "mo1.h"
#include "Cpage1.h"
#include "afxdialogex.h"


// Cpage1 对话框

IMPLEMENT_DYNAMIC(Cpage1, CDialogEx)

Cpage1::Cpage1(CWnd* pParent /*=NULL*/)
	: CDialogEx(Cpage1::IDD, pParent)
{

}

Cpage1::~Cpage1()
{
}

void Cpage1::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(Cpage1, CDialogEx)
	ON_WM_ERASEBKGND()
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// Cpage1 消息处理程序


BOOL Cpage1::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	CDC MemDC;  
	//CBitmap对象
	CBitmap Bitmap,*pOldBitmap;  
	//BITMAP句柄
	BITMAP bm;  
	//加载位图
	Bitmap.LoadBitmap(IDB_BITMAP3);  
	//将位图资源与句柄绑定
	Bitmap.GetObject(sizeof(BITMAP),&bm);  
	//创建与内存兼容的DC
	MemDC.CreateCompatibleDC(pDC);  
	//替换原位图
	pOldBitmap=(CBitmap*)(MemDC.SelectObject(&Bitmap));  
	//获取绘制的区域
	CRect rcClient;
	GetClientRect(&rcClient);  
	//绘制到客户区
	pDC->BitBlt(0,0,rcClient.Width(),rcClient.Height(),&MemDC,0,0,SRCCOPY);
	MemDC.SelectObject(pOldBitmap);
	MemDC.DeleteDC();  
	return TRUE;
}


HBRUSH Cpage1::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  在此更改 DC 的任何特性

	if (pWnd == this) 
	{ 
		return m_brBk; 
	} 
	return hbr; 
}


BOOL Cpage1::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	CBitmap bmp; //设置位图背景 111
	bmp.LoadBitmap(IDB_BITMAP3); 
	m_brBk.CreatePatternBrush(&bmp); 
	bmp.DeleteObject(); 
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}
