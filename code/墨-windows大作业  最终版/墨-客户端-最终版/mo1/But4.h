#pragma once


// CBut4

class CBut4 : public CStatic
{
	DECLARE_DYNAMIC(CBut4)

public:
	CBut4();
	virtual ~CBut4();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	bool m_bOverControl;
};


