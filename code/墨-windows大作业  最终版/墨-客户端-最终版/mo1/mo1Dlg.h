
// mo1Dlg.h : 头文件
//
#if !defined(AFX_CSOCKETCLIDLG_H__478F28EE_A14E_47D0_9D6F_1AF712CC6B1D__INCLUDED_)
#define AFX_CSOCKETCLIDLG_H__478F28EE_A14E_47D0_9D6F_1AF712CC6B1D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "Cbutqueding.h"
#include "But1.h"
#include "But2.h"
#include "But3.h"
#include "But4.h"
#include "But5.h"
// Cmo1Dlg 对话框
class Cmo1Dlg : public CDialogEx
{
// 构造
public:
	Cmo1Dlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_MO1_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

public:
	CBrush m_brBk;
	Cbutqueding queding;
	CBut1 but1;
	CBut2 but2;
	CBut3 but3;
	CBut4 but4;
	CBut5 but5;
// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	HRGN BitmapToRegion(HBITMAP hBmp, COLORREF cTransparentColor, COLORREF cTolerance);
	afx_msg LRESULT OnNcHitTest(CPoint point);
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton5();
	CString Ipadd;
	CString Port;
	
public:
	SOCKET clisock;    //网络部分  111
	sockaddr_in cli;
	int count,ee;

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	void connect(void);
	int add;
	void alltime(void);
	CString everytime;
	CStatic m_error;
	CStatic m_wait;
	//CButton m_1;    4444
	//CButton m_2;
	//CButton m_3;
	//CButton m_4;
	//CButton m_5;
	// 发送的条形码  等数据信息       111
	CString Sendmsg;
	int showorno;
	CListCtrl m_list;
	afx_msg void OnBnClickedOk();
	CString nevertime;
	// 各种判断   111
	int free;


	//各种函数 变量222
	void Cmo1Dlg::SavebmpFile(int dpi=0);
	void Cmo1Dlg::BinBarTobmpData(int binbarwid,int lWidthBytes,int barbmphigh,char*binbar,BYTE*bmpdata);

	char st[10][3][8];
	int showtext;
	double barscale;
	char BarText[9];
	char BinBarText[82];
	int ButtonFlags;
	CString filename;
	CString savename;
	int result;
	BYTE* pImage;
	BYTE* m_pGray;
	struct binlen* bl;
	LONG Height;
	LONG Width;


	int freepanduan;
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	//CButton m_OK;
	CButton *pButtonOK;
	CButton *but11;
	CButton *but12;
	CButton *but13;
	CButton *but14;
	CButton *but15;
	void setbuttoncoltor(void);
	void setbuttoncoltor2(void);
};


UINT thread(LPVOID);
#endif