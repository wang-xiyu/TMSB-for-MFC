#pragma once


// CBut1

class CBut1 : public CStatic
{
	DECLARE_DYNAMIC(CBut1)

public:
	CBut1();
	virtual ~CBut1();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	bool m_bOverControl;
};


