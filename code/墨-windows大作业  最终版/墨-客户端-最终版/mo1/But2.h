#pragma once


// CBut2

class CBut2 : public CStatic
{
	DECLARE_DYNAMIC(CBut2)

public:
	CBut2();
	virtual ~CBut2();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	bool m_bOverControl;
};


