// Csend.cpp : 实现文件
//

#include "stdafx.h"
#include "mo1.h"
#include "Csend.h"
#include "afxdialogex.h"


// Csend 对话框

IMPLEMENT_DYNAMIC(Csend, CDialogEx)

Csend::Csend(CWnd* pParent /*=NULL*/)
	: CDialogEx(Csend::IDD, pParent)
{

	m_rate = 3.0;
	m_showtext=1;
	//CenterWindow();
	//HWND HW=GetSafeHwnd();
	//AnimateWindow(GetSafeHwnd(),1000,AW_BLEND); // 淡入窗口
	/*(若不能运行修改下staAfx.h就好了最顶端增加
	#undef WINVER
	#define WINVER 0x500)
	*/       
	m_text = _T("");
}

Csend::~Csend()
{
}

void Csend::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	//  DDX_Text(pDX, IDC_EDIT1, m_msg);
	DDX_Text(pDX, IDC_EDIT_Rate, m_rate);
	DDX_Check(pDX, IDC_CHECK1, m_showtext);
	DDX_Text(pDX, IDC_EDIT1, m_text);
	DDV_MaxChars(pDX, m_text, 7);
}


BEGIN_MESSAGE_MAP(Csend, CDialogEx)
	ON_BN_CLICKED(IDOK, &Csend::OnBnClickedOk)
END_MESSAGE_MAP()


// Csend 消息处理程序


void Csend::OnBnClickedOk()
{
		/*计算第8位，校验位2222222*/
	UpdateData(TRUE);
    if((len = m_text.GetLength()) == 7)
	{
		/*申明时char换成TCHAR 
			字符串处理函数也要换 
			atoi换成_ttoi，strlen换成_tcslen等等*/
	    strcpy(text,(char*)m_text.GetBuffer (len));
		int val = 0;
		val = text[0] - '0' + text[2] - '0' 
			  + text[4] - '0' + text[6] - '0';
		val *= 3;
		val += (text[1] + text[3] + text[5] - 3 * '0');
		int dec = 10;
		while(dec < val)
			dec += 10;
		text[7] = dec - val + '0';
		text[8] = '\0';
		CDialogEx::OnOK();
	}
	else
		AfxMessageBox(":出错了:>_<:: 请输入7位有效数字");
	

}
