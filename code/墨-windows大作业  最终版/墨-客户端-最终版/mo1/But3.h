#pragma once


// CBut3

class CBut3 : public CStatic
{
	DECLARE_DYNAMIC(CBut3)

public:
	CBut3();
	virtual ~CBut3();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	bool m_bOverControl;
};


