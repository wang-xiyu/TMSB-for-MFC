
// mo1Dlg.cpp : 实现文件
//

#include "stdafx.h"
#include "mo1.h"
#include "mo1Dlg.h"
#include "Cpage1.h"
#include "Cipset.h"
#include "Csend.h"
#include "afxdialogex.h"

#ifdef _DEBUG
  #define new DEBUG_NEW
#endif


// Cmo1Dlg 对话框
BOOL SaveBmp(HBITMAP hBitmap, CString FileName);
unsigned char * Load(const char * fileName, LONG &wid, LONG &hi);
void RGBToGray(unsigned char * image, int len, unsigned char * gray, int wid,
  int hi);
void BiGray(unsigned char * gray, int wid, int hi);
void GetBBlenLink(BYTE * pGray, int wid, int hi, struct binlen *  &bl);
void GetBinCode(struct binlen * bl, char * bincode);
void GetBarText(char st[][3][8], char * binbar, char * bartext);




Cmo1Dlg::Cmo1Dlg(CWnd * pParent /*=NULL*/): CDialogEx(Cmo1Dlg::IDD, pParent),
  Ipadd(_T("")), Port(_T("")), add(0), everytime(_T("")), Sendmsg(_T("")),
  showorno(0), nevertime(_T("")), free(0)
  , freepanduan(0)
  , pButtonOK(NULL)
  , but11(NULL)
  , but12(NULL)
  , but13(NULL)
  , but14(NULL)
  , but15(NULL)

{
  m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);


  showtext = 1;
  barscale = 2.0;
  m_pGray = NULL;
  strcpy(BarText, "");
  strcpy(BinBarText, "");
  strcpy(st[0][0], "0001101");
  strcpy(st[1][0], "0011001");
  strcpy(st[2][0], "0010011");
  strcpy(st[3][0], "0111011");
  strcpy(st[4][0], "0100011");
  strcpy(st[5][0], "0110001");
  strcpy(st[6][0], "0101111");
  strcpy(st[7][0], "0111011");
  strcpy(st[8][0], "0110111");
  strcpy(st[9][0], "0001011");

  strcpy(st[0][1], "0100111");
  strcpy(st[1][1], "0110011");
  strcpy(st[2][1], "0011011");
  strcpy(st[3][1], "0100001");
  strcpy(st[4][1], "0011101");
  strcpy(st[5][1], "0111001");
  strcpy(st[6][1], "0000101");
  strcpy(st[7][1], "0010001");
  strcpy(st[8][1], "0001001");
  strcpy(st[9][1], "0010111");

  strcpy(st[0][2], "1110010");
  strcpy(st[1][2], "1100110");
  strcpy(st[2][2], "1101100");
  strcpy(st[3][2], "1000010");
  strcpy(st[4][2], "1011100");
  strcpy(st[5][2], "1001110");
  strcpy(st[6][2], "1010000");
  strcpy(st[7][2], "1000100");
  strcpy(st[8][2], "1001000");
  strcpy(st[9][2], "1110100");


}

void Cmo1Dlg::DoDataExchange(CDataExchange * pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_error, m_error);
	DDX_Control(pDX, IDC_wait, m_wait);
	//DDX_Control(pDX, IDC_BUTTON1, m_1);   4444
	//DDX_Control(pDX, IDC_BUTTON2, m_2);
	//DDX_Control(pDX, IDC_BUTTON3, m_3);
	//DDX_Control(pDX, IDC_BUTTON4, m_4);
	//DDX_Control(pDX, IDC_BUTTON5, m_5);
	DDX_Control(pDX, IDC_LIST1, m_list);
	//DDX_Control(pDX, IDOK, m_OK);
}

//-------------------------------------------------------------------------

BEGIN_MESSAGE_MAP(Cmo1Dlg, CDialogEx)ON_WM_PAINT()ON_WM_QUERYDRAGICON()
  ON_WM_ERASEBKGND()ON_WM_CTLCOLOR()ON_WM_NCHITTEST()ON_BN_CLICKED(IDC_BUTTON1,
  &Cmo1Dlg::OnBnClickedButton1)ON_BN_CLICKED(IDC_BUTTON2, &Cmo1Dlg
  ::OnBnClickedButton2)ON_BN_CLICKED(IDC_BUTTON3, &Cmo1Dlg::OnBnClickedButton3)
  ON_BN_CLICKED(IDC_BUTTON4, &Cmo1Dlg::OnBnClickedButton4)ON_BN_CLICKED
  (IDC_BUTTON5, &Cmo1Dlg::OnBnClickedButton5)ON_WM_CREATE()ON_WM_DESTROY()
  ON_WM_MOUSEMOVE()
   ON_BN_CLICKED(IDOK, &Cmo1Dlg::OnBnClickedOk)END_MESSAGE_MAP()


// Cmo1Dlg 消息处理程序

BOOL Cmo1Dlg::OnInitDialog()
{
  CDialogEx::OnInitDialog();


  pButtonOK = (CButton*)GetDlgItem(IDOK);
  queding.SubclassDlgItem(IDOK,this);
  
  but11 = (CButton*)GetDlgItem(IDC_BUTTON1);
  but1.SubclassDlgItem(IDC_BUTTON1,this);

  but12 = (CButton*)GetDlgItem(IDC_BUTTON2);
  but2.SubclassDlgItem(IDC_BUTTON2,this);

  but13 = (CButton*)GetDlgItem(IDC_BUTTON3);
  but3.SubclassDlgItem(IDC_BUTTON3,this);

  but14 = (CButton*)GetDlgItem(IDC_BUTTON4);
  but4.SubclassDlgItem(IDC_BUTTON4,this);

  but15 = (CButton*)GetDlgItem(IDC_BUTTON5);
  but5.SubclassDlgItem(IDC_BUTTON5,this);
  
  //pButtonOK = (CButton*)GetDlgItem(IDOK);
  //queding.SubclassDlgItem(IDOK,this);
  //**************进行 IP文件判断 是否曾经登陆成功，进而判断是否弹出IP填写框
  int a = 0;
  CStdioFile F;
  CStdioFile sfile;
  if (!F.Open("C:\\IPadd.txt", CFile::modeRead))
  //第一次在此台计算器使用
  {
    a = 1;
    if (a == 1)
    {
      a = 2;
      Cipset dlg;
      if (dlg.DoModal() == IDOK)
      {
        Ipadd = dlg.m_ipadd; //获得IP
        Port = dlg.m_port; //获得端口
      }
      else
      {
        exit(1);
      }
      CStdioFile F;
      CString str = "first-try";
      CString str1 = "C:\\IPadd.txt";
      F.Open(str1, CFile::modeCreate | CFile::modeWrite | CFile::modeRead |
        CFile::typeText);
      F.Write(str, str.GetLength());
      F.Close();
    }


  }
  if (a != 2)
  {


    CString strLine;
    if (F.ReadString(strLine))
    //存在读取第一行
    {

      if (strLine == "first-try")
      //上次登录未成功
      {
        Cipset dlg;
        if (dlg.DoModal() == IDOK)
        {
          Ipadd = dlg.m_ipadd; //获得IP
          Port = dlg.m_port; //获得端口
        }
        else
        {
          return FALSE;
        }
      }
      else
      {

        Ipadd = strLine; //上一次进行了成功登陆;				
      }
    }
  }



  connect(); //进行连接尝试
  //加载位图背景  后裁剪111
  CBitmap bmp1;
  if (bmp1.LoadBitmap(IDB_BITMAP1))
  {
    HRGN rgn;
    rgn = BitmapToRegion((HBITMAP)bmp1, RGB(255, 255, 255), RGB(255, 255, 255));
    SetWindowRgn(rgn, TRUE);
    bmp1.DeleteObject();
  }
  // 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
  //  执行此操作
  SetIcon(m_hIcon, TRUE); // 设置大图标
  SetIcon(m_hIcon, FALSE); // 设置小图标

  // TODO: 在此添加额外的初始化代码
  //设置位图按钮
  CBitmap Bitmap;
  Bitmap.LoadBitmap(IDB_BITMAP2);
  HBITMAP hBitmap = (HBITMAP)Bitmap.Detach();
  CButton * pButton = (CButton*)GetDlgItem(IDOK);
  pButton->SetBitmap(hBitmap);

  CBitmap Bitmap1;
  Bitmap1.LoadBitmap(IDB_BITMAP6);
  HBITMAP hBitmap1 = (HBITMAP)Bitmap1.Detach();
  CButton * pButton1 = (CButton*)GetDlgItem(IDC_BUTTON1);
  pButton1->SetBitmap(hBitmap1);

  CBitmap Bitmap2;
  Bitmap2.LoadBitmap(IDB_BITMAP7);
  HBITMAP hBitmap2 = (HBITMAP)Bitmap2.Detach();
  CButton * pButton2 = (CButton*)GetDlgItem(IDC_BUTTON2);
  pButton2->SetBitmap(hBitmap2);

  CBitmap Bitmap3;
  Bitmap3.LoadBitmap(IDB_BITMAP4);
  HBITMAP hBitmap3 = (HBITMAP)Bitmap3.Detach();
  CButton * pButton3 = (CButton*)GetDlgItem(IDC_BUTTON3);
  pButton3->SetBitmap(hBitmap3);

  CBitmap Bitmap4;
  Bitmap4.LoadBitmap(IDB_BITMAP8);
  HBITMAP hBitmap4 = (HBITMAP)Bitmap4.Detach();
  CButton * pButton4 = (CButton*)GetDlgItem(IDC_BUTTON4);
  pButton4->SetBitmap(hBitmap4);

  CBitmap Bitmap5;
  Bitmap5.LoadBitmap(IDB_BITMAP5);
  HBITMAP hBitmap5 = (HBITMAP)Bitmap5.Detach();
  CButton * pButton5 = (CButton*)GetDlgItem(IDC_BUTTON5);
  pButton5->SetBitmap(hBitmap5);
  // TODO: 在此添加额外的初始化代码
  //设置位图背景 111
  UpdateWindow();
  CBitmap bmp;
  bmp.LoadBitmap(IDB_BITMAP1);
  m_brBk.CreatePatternBrush(&bmp);
  bmp.DeleteObject();


  count = 0;
  m_list.InsertColumn(0,
    "序列    条码        时间                            结果");
  m_list.SetColumnWidth(0, 400);

  //网络连接出错画面111
  /*
  Cpage1 *page1;
  page1=new Cpage1;
  page1->Create(IDD_DIALOG1,this);
  page1->ShowWindow(SW_SHOW);
  //CRect rect;
  //AfxGetMainWnd()->GetWindowRect(&rect);
  page1->MoveWindow(115,66,356,240,TRUE);
   */
  /*
  SetWindowPos(page1,0,0,0,0, SWP_NOMOVE | SWP_NOSIZE);

  GetDlgItem(IDC_BUTTON1)->ShowWindow(FALSE);
  GetDlgItem(IDC_BUTTON2)->ShowWindow(FALSE);
  GetDlgItem(IDC_BUTTON3)->ShowWindow(FALSE);
  GetDlgItem(IDC_BUTTON4)->ShowWindow(FALSE);
  GetDlgItem(IDC_BUTTON5)->ShowWindow(FALSE);

   */
  return TRUE; // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void Cmo1Dlg::OnPaint()
{
  if (IsIconic())
  {
    CPaintDC dc(this); // 用于绘制的设备上下文

    SendMessage(WM_ICONERASEBKGND, reinterpret_cast < WPARAM > (dc.GetSafeHdc())
      , 0);

    // 使图标在工作区矩形中居中
    int cxIcon = GetSystemMetrics(SM_CXICON);
    int cyIcon = GetSystemMetrics(SM_CYICON);
    CRect rect;
    GetClientRect(&rect);
    int x = (rect.Width() - cxIcon + 1) / 2;
    int y = (rect.Height() - cyIcon + 1) / 2;

    // 绘制图标
    dc.DrawIcon(x, y, m_hIcon);
  }
  else
  {
    CDialogEx::OnPaint();
  }
  CDC * pDC = GetDC();
  if (ButtonFlags == 1)
  {
    if (pImage == NULL)
    {
      return ;
    }
    BITMAPINFOHEADER bmiHeader;
    bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
    bmiHeader.biWidth = Width;
    bmiHeader.biHeight = Height;
    bmiHeader.biPlanes = 1;
    bmiHeader.biBitCount = 24;
    bmiHeader.biCompression = BI_RGB;
    bmiHeader.biSizeImage = 0;
    bmiHeader.biXPelsPerMeter = 0;
    bmiHeader.biYPelsPerMeter = 0;
    bmiHeader.biClrUsed = 0;
    bmiHeader.biClrImportant = 0;

    CRect rect;
    GetDlgItem(IDC_LIST1)->GetWindowRect(&rect);
    ScreenToClient(rect); //转换相对于对话框的坐标
    CBrush brush(RGB(255, 255, 255));
    pDC->FillRect(&rect, &brush);
    pDC->SetStretchBltMode(COLORONCOLOR); //删除所有消除的像素行
    if (rect.Width() > Width && rect.Height() > Height)
    {
      StretchDIBits(pDC->m_hDC, rect.Width()/2+30, rect.Height()/2-30, bmiHeader.biWidth,
        bmiHeader.biHeight, 0, 0, bmiHeader.biWidth, bmiHeader.biHeight, pImage,
        (BITMAPINFO*) &bmiHeader, DIB_RGB_COLORS, SRCCOPY);
    }
    else
    {
      StretchDIBits(pDC->m_hDC, rect.left, rect.top, rect.Width(), rect.Height()
        , 0, 0, bmiHeader.biWidth, bmiHeader.biHeight, pImage, (BITMAPINFO*)
        &bmiHeader, DIB_RGB_COLORS, SRCCOPY);
    }

  }
  else if (ButtonFlags == 2){}
  else if (ButtonFlags == 3)
  {
    int i;
    char bText[2] =
    {
      0
    };
    CString str;
    CFont font;
    double modWidth;

    CRect rect;
    GetDlgItem(IDC_LIST1)->GetWindowRect(&rect); //获得静态框的大小
    ScreenToClient(rect); //转换相对于对话框的坐标
    CBrush brush(RGB(255, 255, 255));
    pDC->FillRect(&rect, &brush);
    modWidth = 33 * barscale;
    int StartX = (rect.right + rect.left) / 2 * 0.33 * 100-81 * 0.33 * 100 *
      barscale / 2+15*0.33*100;
    int StartY =  - (rect.bottom + rect.top) / 2 * 0.33 * 100+21.64 * 100 *
      barscale / 2-15*33;
    pDC->SetMapMode(MM_HIMETRIC);
    //每个逻辑单位转换为0.01毫米(默认1像素)，X正方向右(默认右)，Y的正方向上(默认下)。

    int oldStartX = StartX;

    CPen newPen(PS_SOLID, 1, RGB(0, 0, 0));
    pDC->SelectObject(&newPen);
    if (strlen(BinBarText) > 0)
    {
      pDC->Rectangle(&CRect(StartX - 1, StartY + 1, StartX + 81 * modWidth + 1,
        StartY - 21.64 * 100 * barscale - 5));
      CBrush brush(RGB(0, 0, 0));
      for (i = 0; i < strlen(BinBarText); i++)
      {
        if (BinBarText[i] == '1')
        {
          if (i == 7 || i == 9 || i == 39 || i == 41 || i == 71 || i == 73)
          {
            pDC->FillRect(&CRect(StartX, StartY - 33 * barscale, StartX +
              modWidth, StartY - (33+19.88 * 100) * barscale), &brush);
          }
          else
          {
            pDC->FillRect(&CRect(StartX, StartY - 33 * barscale, StartX +
              modWidth, StartY - (33+18.23 * 100) * barscale), &brush);
          }
        }
        StartX += modWidth;
      }
      if (showtext)
      {
        pDC->SetMapMode(MM_TWIPS);
        //每个逻辑单位转换为打印点的1/20（即1/1400英寸），X正方向向右，Y方向向上。
        StartX = oldStartX;
        font.CreateFont( - 125 * barscale, 0, 0, 0, 400, FALSE, FALSE, 0,
          ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
          DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, "Times New Roman");
        CFont * pOldFont = (CFont*)pDC->SelectObject(&font);
        StartX = (StartX + 2 * 33 * barscale) * 0.567;
        StartY = (StartY - (0.33 + 18.56) * 100 * barscale) * 0.567;
        bText[0] = '<';
        pDC->TextOut(StartX, StartY, bText);
        StartX += 4 * 33 * barscale * 0.567;
        for (i = 0; i <= 3; i++)
        {
          bText[0] = BarText[i];
          StartX += 7 * 33 * barscale * 0.567;
          pDC->TextOut(StartX, StartY, bText);
        }
        StartX += 4 * 33 * barscale * 0.567;
        for (i = 4; i <= 7; i++)
        {
          bText[0] = BarText[i];
          StartX += 7 * 33 * barscale * 0.567;
          pDC->TextOut(StartX, StartY, bText);
        }
        StartX += 10 * 33 * barscale * 0.567;
        bText[0] = '>';
        pDC->TextOut(StartX, StartY, bText);
        pDC->SelectObject(pOldFont);
      }
    }
    pDC->SetMapMode(MM_TEXT);
  }
  ReleaseDC(pDC);


}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR Cmo1Dlg::OnQueryDragIcon()
{
  return static_cast < HCURSOR > (m_hIcon);
}

//-------------------------------------------------------------------------



BOOL Cmo1Dlg::OnEraseBkgnd(CDC * pDC) //设置对话框背景111
{
  // TODO: 在此添加消息处理程序代码和/或调用默认值
  CDC MemDC;
  //CBitmap对象
  CBitmap Bitmap,  * pOldBitmap;
  //BITMAP句柄
  BITMAP bm;
  //加载位图
  Bitmap.LoadBitmap(IDB_BITMAP1);
  //将位图资源与句柄绑定
  Bitmap.GetObject(sizeof(BITMAP), &bm);
  //创建与内存兼容的DC
  MemDC.CreateCompatibleDC(pDC);
  //替换原位图
  pOldBitmap = (CBitmap*)(MemDC.SelectObject(&Bitmap));
  //获取绘制的区域
  CRect rcClient;
  GetClientRect(&rcClient);
  //绘制到客户区
  pDC->BitBlt(0, 0, rcClient.Width(), rcClient.Height(), &MemDC, 0, 0, SRCCOPY);
  MemDC.SelectObject(pOldBitmap);
  MemDC.DeleteDC();
  return TRUE; // return TRUE unless you set the focus to a control
}

//-------------------------------------------------------------------------


HBRUSH Cmo1Dlg::OnCtlColor(CDC * pDC, CWnd * pWnd, UINT nCtlColor)
//设置对话框背景111
{
  HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

  // TODO:  在此更改 DC 的任何特性
  if (pWnd == this)
  {
    return m_brBk;
  }
  return hbr;
}

//-------------------------------------------------------------------------



UINT thread(LPVOID v) //网络连接线程111
{
  char buff[100];
  char array[25][30] =
  {
    "155.245.160.151", "155.245.160.152", "155.245.160.153", "155.245.160.154",
      "155.245.160.155", "155.245.160.156", "155.245.160.157",
      "155.245.160.158", "155.245.160.159", "155.245.160.160",
      "155.245.160.161", "155.245.160.162", "155.245.160.163",
      "155.245.160.164", "155.245.160.165", "155.245.160.166",
      "155.245.160.167", "155.245.160.168", "155.245.160.169",
      "155.245.160.170", "155.245.160.171", "155.245.160.172",
      "155.245.160.173", "155.245.160.174", "155.245.160.175"
  };
  CSize size;
  size.cx = 0;
  size.cy = 30;
  int s = 1, addcount = 0;
  Cmo1Dlg * dlg = (Cmo1Dlg*)AfxGetApp()->GetMainWnd();

  //连接到服务器
  if (connect(dlg->clisock, (sockaddr*) &(dlg->cli), sizeof(dlg->cli)) && dlg
    ->ee != 0)
  {
    //dlg->m_edit.SetWindowText("等待.....");
    //空循环	

    //****************连接失败    111
    dlg->m_wait.ShowWindow(SW_HIDE);
    dlg->m_error.ShowWindow(SW_SHOW);
    AfxMessageBox(
      "失败了！::>_<:: 可能原因包括服务器未打开和网络繁忙。请稍候再试");

    //dlg->m_7.EnableWindow(FALSE);
    //dlg->m_6.SetWindowText("建立连接");
    return 0;
  }
  else if (dlg->ee == 1)
  //连接成功
  {
    //
    Sleep(2000);
    dlg->UpdateWindow();
    dlg->but11->ShowWindow(SW_SHOW);
    dlg->but12->ShowWindow(SW_SHOW);
    dlg->but13->ShowWindow(SW_SHOW);
    dlg->but14->ShowWindow(SW_SHOW);
    dlg->but15->ShowWindow(SW_SHOW);
   // dlg->UpdateWindow();
    dlg->m_wait.ShowWindow(SW_HIDE);
    dlg->m_error.ShowWindow(SW_HIDE);
    dlg->UpdateWindow();
    
	
	dlg->setbuttoncoltor2();   //青按钮
	
	dlg->UpdateWindow();
	Sleep(500);
	dlg->setbuttoncoltor();   //白按钮
	//AfxMessageBox("连接成功");
	 dlg->UpdateWindow();
    //dlg->m_6.EnableWindow(false);
    //dlg->m_7.EnableWindow(TRUE);
    //dlg->m_6.SetWindowText("已经连接服务器");
    CStdioFile F;
    CString str1 = "C:\\IPadd.txt"; //成功登陆后写入IPadd地址文件
    F.Open(str1, CFile::modeCreate | CFile::modeWrite | CFile::modeRead | CFile
      ::typeText);
    F.Write(dlg->Ipadd, dlg->Ipadd.GetLength());
    F.Close();
  }

  dlg->SetForegroundWindow(); //置顶
  dlg->alltime();
  //循环获得数据
  while (s != SOCKET_ERROR && dlg->ee != 0)
  {
    //调用recv函数接收数据
    s = recv(dlg->clisock, buff, 100, 0);
    CString str, str1;
    str = buff;
    if (str == "服务器已退出")
    {
		
     dlg->but11->ShowWindow(SW_HIDE);
      dlg->but12->ShowWindow(SW_HIDE);
      dlg->but13->ShowWindow(SW_HIDE);
      dlg->but14->ShowWindow(SW_HIDE);
      dlg->but15->ShowWindow(SW_HIDE);
      dlg->m_wait.ShowWindow(SW_HIDE);
      dlg->m_error.ShowWindow(SW_SHOW);
      dlg->m_list.ShowWindow(SW_HIDE);
      AfxMessageBox("服务器意外终止了。请稍候重新再连接");
      return 0;
    }
   
    dlg->add++;
    str1.Format("%d", dlg->add);
    dlg->SetForegroundWindow();
    if (s != SOCKET_ERROR && dlg->ee != 0)
    {
      dlg->setbuttoncoltor();
      if (dlg->free == 1)
      //表示由查询发送   然后反馈回来的   111
      { 
		  dlg->setbuttoncoltor();
		 // AfxMessageBox(str);
		  if(str=="::>_<:: 居然没有找到")
		  {
			  AfxMessageBox("::>_<:: 居然没有找到");
			  
		  }
		  else
		  {
			  CString str2;
			  str2="^_^找到了\n";
			  str2+="条码：";
			  str2+=dlg->Sendmsg;
			  str2+="\n";
			  str2+="匹对信息：";
			  str2+=str ;
			  str2+="\n";
			  str2+="查询资料已记录到列表框";
			  AfxMessageBox(str2);		 
		  }

        dlg->m_list.InsertItem(dlg->count++, str1 + "     " + dlg->Sendmsg
          + "    " + dlg->nevertime + "    " + buff);
        dlg->m_list.Scroll(size);
        dlg->free = 0;

      }
      else
      //由系统发送   的系统消息
      {
		   AfxMessageBox(str);
        dlg->m_list.InsertItem(dlg->count++, str1 + "    " + "服务器消息" +
          "   " + dlg->nevertime + "           " + buff);
        dlg->m_list.Scroll(size);
      }

      dlg->UpdateWindow();
    }
  }
  //发送断开命令
  send(dlg->clisock, "断开连接", 100, 0);
  closesocket(dlg->clisock);
  AfxEndThread(0);
  return 0;
}

//-------------------------------------------------------------------------





HRGN Cmo1Dlg::BitmapToRegion(HBITMAP hBmp, COLORREF cTransparentColor, COLORREF
  cTolerance)
{
  HRGN hRgn = NULL;

  if (hBmp)
  {
    HDC hMemDC = CreateCompatibleDC(NULL);
    if (hMemDC)
    {
      BITMAP bm;
      GetObject(hBmp, sizeof(bm), &bm);

      //创建一个32位色的位图，并选进内存设备环境
      BITMAPINFOHEADER RGB32BITSBITMAPINFO =
      {
        sizeof(BITMAPINFOHEADER),  // biSize
        bm.bmWidth,  // biWidth;
        bm.bmHeight,  // biHeight;
        1,  // biPlanes;
        32,  // biBitCount
        BI_RGB,  // biCompression;
        0,  // biSizeImage;
        0,  // biXPelsPerMeter;
        0,  // biYPelsPerMeter;
        0,  // biClrUsed;
        0  // biClrImportant;
      };
      VOID * pbits32;
      HBITMAP hbm32 = CreateDIBSection(hMemDC, (BITMAPINFO*)
        &RGB32BITSBITMAPINFO, DIB_RGB_COLORS, &pbits32, NULL, 0);
      if (hbm32)
      {
        HBITMAP holdBmp = (HBITMAP)SelectObject(hMemDC, hbm32);

        // Create a DC just to copy the bitmap into the memory DC
        HDC hDC = CreateCompatibleDC(hMemDC);
        if (hDC)
        {
          // Get how many bytes per row we have for the bitmap bits (rounded up to 32 bits)
          BITMAP bm32;
          GetObject(hbm32, sizeof(bm32), &bm32);
          while (bm32.bmWidthBytes % 4)
          {
            bm32.bmWidthBytes++;
          }

          // Copy the bitmap into the memory DC
          HBITMAP holdBmp = (HBITMAP)SelectObject(hDC, hBmp);
          BitBlt(hMemDC, 0, 0, bm.bmWidth, bm.bmHeight, hDC, 0, 0, SRCCOPY);

          // For better performances, we will use the ExtCreateRegion() function to create the
          // region. This function take a RGNDATA structure on entry. We will add rectangles by
          // amount of ALLOC_UNIT number in this structure.
          #define ALLOC_UNIT    100
          DWORD maxRects = ALLOC_UNIT;
          HANDLE hData = GlobalAlloc(GMEM_MOVEABLE, sizeof(RGNDATAHEADER) +
            (sizeof(RECT) * maxRects));
          RGNDATA * pData = (RGNDATA*)GlobalLock(hData);
          pData->rdh.dwSize = sizeof(RGNDATAHEADER);
          pData->rdh.iType = RDH_RECTANGLES;
          pData->rdh.nCount = pData->rdh.nRgnSize = 0;
          SetRect(&pData->rdh.rcBound, MAXLONG, MAXLONG, 0, 0);

          // Keep on hand highest and lowest values for the "transparent" pixels
          BYTE lr = GetRValue(cTransparentColor);
          BYTE lg = GetGValue(cTransparentColor);
          BYTE lb = GetBValue(cTransparentColor);
          BYTE hr = min(0xff, lr + GetRValue(cTolerance));
          BYTE hg = min(0xff, lg + GetGValue(cTolerance));
          BYTE hb = min(0xff, lb + GetBValue(cTolerance));

          // Scan each bitmap row from bottom to top (the bitmap is inverted vertically)
          BYTE * p32 = (BYTE*)bm32.bmBits + (bm32.bmHeight - 1) *
            bm32.bmWidthBytes;
          for (int y = 0; y < bm.bmHeight; y++)
          {
            // Scan each bitmap pixel from left to right
            for (int x = 0; x < bm.bmWidth; x++)
            {
              // Search for a continuous range of "non transparent pixels"
              int x0 = x;
              LONG * p = (LONG*)p32 + x;
              while (x < bm.bmWidth)
              {
                BYTE b = GetRValue(*p);
                if (b >= lr && b <= hr)
                {
                  b = GetGValue(*p);
                  if (b >= lg && b <= hg)
                  {
                    b = GetBValue(*p);
                    if (b >= lb && b <= hb)
                    // This pixel is "transparent"
                    {
                      break;
                    }
                  }
                }
                p++;
                x++;
              }

              if (x > x0)
              {
                // Add the pixels (x0, y) to (x, y+1) as a new rectangle in the region
                if (pData->rdh.nCount >= maxRects)
                {
                  GlobalUnlock(hData);
                  maxRects += ALLOC_UNIT;
                  hData = GlobalReAlloc(hData, sizeof(RGNDATAHEADER) + (sizeof
                    (RECT) * maxRects), GMEM_MOVEABLE);
                  pData = (RGNDATA*)GlobalLock(hData);
                }
                RECT * pr = (RECT*) &pData->Buffer;
                SetRect(&pr[pData->rdh.nCount], x0, y, x, y + 1);
                if (x0 < pData->rdh.rcBound.left)
                {
                  pData->rdh.rcBound.left = x0;
                }
                if (y < pData->rdh.rcBound.top)
                {
                  pData->rdh.rcBound.top = y;
                }
                if (x > pData->rdh.rcBound.right)
                {
                  pData->rdh.rcBound.right = x;
                }
                if (y + 1 > pData->rdh.rcBound.bottom)
                {
                  pData->rdh.rcBound.bottom = y + 1;
                }
                pData->rdh.nCount++;

                // On Windows98, ExtCreateRegion() may fail if the number of rectangles is too
                // large (ie: > 4000). Therefore, we have to create the region by multiple steps.
                if (pData->rdh.nCount == 2000)
                {
                  HRGN h = ExtCreateRegion(NULL, sizeof(RGNDATAHEADER) +
                    (sizeof(RECT) * maxRects), pData);
                  if (hRgn)
                  {
                    CombineRgn(hRgn, hRgn, h, RGN_OR);
                    DeleteObject(h);
                  }
                  else
                  {
                    hRgn = h;
                  }
                  pData->rdh.nCount = 0;
                  SetRect(&pData->rdh.rcBound, MAXLONG, MAXLONG, 0, 0);
                }
              }
            }

            // Go to next row (remember, the bitmap is inverted vertically)
            p32 -= bm32.bmWidthBytes;
          }

          // Create or extend the region with the remaining rectangles
          HRGN h = ExtCreateRegion(NULL, sizeof(RGNDATAHEADER) + (sizeof(RECT)
            *maxRects), pData);
          if (hRgn)
          {
            CombineRgn(hRgn, hRgn, h, RGN_OR);
            DeleteObject(h);
          }
          else
          {
            hRgn = h;
          }

          // Clean up
          GlobalFree(hData);
          SelectObject(hDC, holdBmp);
          DeleteDC(hDC);
        }
        DeleteObject(SelectObject(hMemDC, holdBmp));
      }
      DeleteDC(hMemDC);
    }
  }
  return hRgn;
}

//-------------------------------------------------------------------------


LRESULT Cmo1Dlg::OnNcHitTest(CPoint point)
{
  // TODO: 在此添加消息处理程序代码和/或调用默认值
  UINT nValue = CDialog::OnNcHitTest(point);
  if (nValue == HTCLIENT)
  {
    nValue = HTCAPTION;
  }
  return nValue;
  return CDialogEx::OnNcHitTest(point);
}

//-------------------------------------------------------------------------


void Cmo1Dlg::OnBnClickedButton1() //导入和扫描111
{
  // 导入扫描2222
  //AfxMessageBox("导入和扫描");
	setbuttoncoltor();
	//UpdateWindow();
  CString filter;
  filter = "位图(*.bmp)|*.bmp|All Files(*.*)|*.*||";

  CFileDialog file_open_dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY |
    OFN_FILEMUSTEXIST, filter); //OFN_FILEMUSTEXIST不存在文件名继续等待
  file_open_dlg.m_ofn.lpstrTitle = "别拖拖拉拉的，快打开";
  if (file_open_dlg.DoModal() == IDOK)
  {
    filename = file_open_dlg.GetPathName();
    ButtonFlags = 1;
    pImage = Load(filename, Width, Height);



    //灰度化
    if (m_pGray != NULL)
    {
      delete m_pGray;
    }
    m_pGray = new unsigned char[Width * Height];
    unsigned char * gray;
    gray = m_pGray;
    int len = Width * 3;
    len += (4-len % 4) % 4;
    len *= Height;
    RGBToGray(pImage, len, gray, Width, Height);
    for (int i = 0; i < Height; i++)
    for (int j = 0; j < Width; j++)
    {
      //if(gray[i*Width+j]==0){
      pImage[(Height - 1-i)*(len / Height) + 3 * j] = gray[i * Width + j];
      pImage[(Height - 1-i)*(len / Height) + 3 * j + 1] = gray[i * Width + j];
      pImage[(Height - 1-i)*(len / Height) + 3 * j + 2] = gray[i * Width + j];
    }


    //二值化
    BiGray(m_pGray, Width, Height);
    len = Width * 3;
    len += (4-len % 4) % 4;
    len *= Height;
    for (int i = 0; i < Height; i++)
    for (int j = 0; j < Width; j++)
    {
      //if(gray[i*Width+j]==0){
      pImage[(Height - 1-i)*(len / Height) + 3 * j] = m_pGray[i * Width + j];
      pImage[(Height - 1-i)*(len / Height) + 3 * j + 1] = m_pGray[i * Width +
        j];
      pImage[(Height - 1-i)*(len / Height) + 3 * j + 2] = m_pGray[i * Width +
        j];
    }

    //得到识别结果
    GetBBlenLink(m_pGray, Width, Height, bl);
    GetBinCode(bl, BinBarText);
    //AfxMessageBox(BinBarText);
    GetBarText(st, BinBarText, BarText);
    AfxMessageBox(BarText);
    Sendmsg.Format("%s", BarText);
    Invalidate();
  }

}

//-------------------------------------------------------------------------


void Cmo1Dlg::OnBnClickedButton2() //手动输入111
{
  // 手动输入222
	setbuttoncoltor();
	//UpdateWindow();
  Csend gdlg;
  if (gdlg.DoModal() == IDOK && gdlg.len == 7)
  //输入7个数字才执行
  {
    ButtonFlags = 3;
    Sendmsg = gdlg.m_text;
    strcpy(BarText, gdlg.text); //得到8位码
    result = atoi(gdlg.text); //接收用户输入条形码，发送到服务端

    barscale = gdlg.m_rate;
    showtext = gdlg.m_showtext;
    strcpy(BinBarText, "");
    int i;
    /*把8位码转换成二进制*/
    if (strlen(BarText) != 0)
    {
      strcpy(BinBarText, "0000000");
      strcat(BinBarText, "101");
      for (i = 0; i <= 3; i++)
      {
        strcat(BinBarText, st[BarText[i] - '0'][0]);
      }
      strcat(BinBarText, "01010");
      for (i = 4; i <= 7; i++)
      {
        strcat(BinBarText, st[BarText[i] - '0'][2]);
      }
      strcat(BinBarText, "101");
      strcat(BinBarText, "0000000");
    }
  }
  Invalidate();
  //GetDlgItem(IDC_BUTTON1)->EnableWindow(TRUE);
}

//-------------------------------------------------------------------------



void Cmo1Dlg::OnBnClickedButton3() //联网查询111
{
  // TODO: 在此添加控件通知处理程序代码
	setbuttoncoltor();
	//UpdateWindow();
	SECURITY_ATTRIBUTES sa;   
	HANDLE hRead,hWrite;   

	sa.nLength = sizeof(SECURITY_ATTRIBUTES);   
	sa.lpSecurityDescriptor = NULL;   
	sa.bInheritHandle = TRUE;   

	if(!CreatePipe(&hRead,&hWrite,&sa,0))   
	{   
		//MessageBox("CreatePipe Failed");   
		return;   
	}   

	STARTUPINFO si;   
	PROCESS_INFORMATION pi;   

	ZeroMemory(&si,sizeof(STARTUPINFO));   
	si.cb = sizeof(STARTUPINFO);   
	GetStartupInfo(&si);   
	si.hStdError = hWrite;   
	si.hStdOutput = hWrite;   
	si.wShowWindow = SW_HIDE;   
	si.dwFlags = STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;   

	char cmdline[200];   
	CString tmp,stredit2;   
	//GetDlgItemText(IDC_EDIT1,stredit2);   //字符传输
	stredit2="ipconfig";
	tmp.Format("cmd /C %s",stredit2);   
	sprintf(cmdline,"%s",tmp);   

	if(!CreateProcess(NULL,cmdline,NULL,NULL,TRUE,NULL,NULL,NULL,&si,&pi))   
	{   
		//MessageBox("CreateProcess failed!");   
		return;   
	}   
	CloseHandle(hWrite);   

	char buffer[4096] = {0};   
	CString strOutput;   
	DWORD bytesRead;   

	while(1)   
	{   
		if(NULL == ReadFile(hRead,buffer,4095,&bytesRead,NULL))   
		{   
			break;   
		}   
		strOutput += buffer;   
		//SetDlgItemText(IDC_EDIT1,strOutput);   //回显
		AfxMessageBox(strOutput);
		char buff[11100];
		strcpy(buff, strOutput);
		send(clisock, buff, 11100, 0);
		//AfxMessageBox(dosreturn);


		UpdateWindow();   
		Sleep(1000);   
	}   
	CloseHandle(hRead);  







	/*
	if (Sendmsg != "")
  {
    free = 1;
    char buff[100];
    CSize size;
    size.cx = 0;
    size.cy = 30;
    strcpy(buff, Sendmsg);
    send(clisock, buff, 100, 0);

  }
  else if(freepanduan==100)
  {
      AfxMessageBox("出错了！::>_<:: 现在不位于条码页");
  }
  else
  {
	  AfxMessageBox("出错了！::>_<:: 哪里都没有发现条形码");
  }
  //AfxMessageBox("联网查询");
  */
}

//-------------------------------------------------------------------------


void Cmo1Dlg::OnBnClickedButton4() //   保存111
{
  // 保存2222
  //AfxMessageBox("保存");
	setbuttoncoltor();
	//UpdateWindow();
  if (ButtonFlags == 3)
  {
    ButtonFlags = 2;
    SavebmpFile();
    Load(savename, Width, Height);

    HWND hDispHwnd = GetDlgItem(IDC_Screen)->GetSafeHwnd();
    HDC dc = ::GetDC(hDispHwnd);
    HDC dcMem;
    //该函数创建一个与指定设备兼容的内存设备上下文环境（DC）。通过GetDc()获取的HDC直接与相关设备沟通
    //而本函数创建的DC，则是与内存中的一个表面相关联。

    dcMem = ::CreateCompatibleDC(dc);
    int i;
    char bText[2] =
    {
      0
    };
    CString str;
    CFont font;

    //该函数创建与指定的设备环境相关的设备兼容的位图。指定位图的宽度，高度 单位为像素。
    CBitmap bmp;
    bmp.Attach(LoadImage(NULL, savename, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));

    HBITMAP hBitmap = (HBITMAP)bmp;
    //HBITMAP	hBitmap   =   ::CreateCompatibleBitmap(dc,Width,20);

    //选入新的保存旧的位图
    HBITMAP hOldBm = (HBITMAP)::SelectObject(dcMem, hBitmap);
    //SetBkMode(dcMem,TRANSPARENT);
    if (showtext)
    {
      SetMapMode(dcMem, MM_TEXT);
      SetBkColor(dcMem, RGB(255, 255, 255)); //设置文字背景
      int StartX = 0;
      int StartY = Height - 28;
      font.CreateFont(20, 0, 0, 0, 400, FALSE, FALSE, 0, ANSI_CHARSET,
        OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH
        | FF_ROMAN, "Times New Roman");
      CFont * pOldFont = (CFont*)SelectObject(dcMem, font);

      StartX += 7;
      bText[0] = '<';
      TextOut(dcMem, StartX, StartY, bText, 1);

      StartX += 13;
      for (i = 0; i <= 3; i++)
      {
        bText[0] = BarText[i];
        StartX += 20;
        TextOut(dcMem, StartX, StartY, bText, 1);
      }
      StartX += 17;
      for (i = 4; i <= 7; i++)
      {
        bText[0] = BarText[i];
        StartX += 20;
        TextOut(dcMem, StartX, StartY, bText, 1);
      }
      StartX += 30;
      bText[0] = '>';
      TextOut(dcMem, StartX, StartY, bText, 1);
      SelectObject(dcMem, pOldFont);
    }
    BitBlt(dc, 0, 0, Width, Height, dcMem, 0, 0, SRCCOPY); //将保存位图立即显示
    SaveBmp(hBitmap, savename);
    ::DeleteDC(dcMem);
    ::ReleaseDC(hDispHwnd, dc);
  }


}

//-------------------------------------------------------------------------


void Cmo1Dlg::OnBnClickedButton5() //历史记录   111
{
  // TODO: 在此添加控件通知处理程序代码
	
	setbuttoncoltor();





  //UpdateWindow();
  if (GetDlgItem(IDC_LIST1)->IsWindowVisible())
  {
	//  GetDlgItem(IDC_BUTTON3)->EnableWindow(TRUE);
	   freepanduan=101;//开启联网查询功能
    GetDlgItem(IDC_LIST1)->ShowWindow(SW_HIDE);
  }
  else
  {
	  // GetDlgItem(IDC_BUTTON3)->EnableWindow(FALSE);  
		 freepanduan=100;//关闭联网查询功能
    GetDlgItem(IDC_LIST1)->ShowWindow(SW_SHOW);
  }
  //UpdateWindow();
  CBut1 * dlg=new CBut1;
  dlg->m_bOverControl=false;
  //AfxMessageBox("历史记录");
}

//-------------------------------------------------------------------------


int Cmo1Dlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
  if (CDialogEx::OnCreate(lpCreateStruct) ==  - 1)
  {
    return  - 1;
  }

  // TODO:  在此添加您专用的创建代码
  ::SetProp(m_hWnd, AfxGetApp()->m_pszExeName, (HANDLE)1);
  return 0;
}

//-------------------------------------------------------------------------


void Cmo1Dlg::OnDestroy()
{
  CDialogEx::OnDestroy();
  ::RemoveProp(m_hWnd, AfxGetApp()->m_pszExeName);
  // TODO: 在此处添加消息处理程序代码
}

//-------------------------------------------------------------------------


void Cmo1Dlg::connect(void)
{
  //初始化socket库      111
  WORD wVersionRequested;
  WSADATA wsaData;
  int err;
  wVersionRequested = MAKEWORD(1, 1);
  err = WSAStartup(wVersionRequested, &wsaData);
  if (err != 0)
  {
    MessageBox("初始化socket库错误");
  }
  if (LOBYTE(wsaData.wVersion) != 1 || HIBYTE(wsaData.wVersion) != 1)
  {
    WSACleanup();
  }

  cli.sin_addr.s_addr = inet_addr(Ipadd);
  cli.sin_family = AF_INET;
  //int   m   =   atoi((const   char*)Port);
  cli.sin_port = 5000; //htons(5000);
  //创建socket
  clisock = socket(AF_INET, SOCK_STREAM, 0);
  ee = 1;
  //启动线程
  AfxBeginThread(thread, 0);

  //GetDlgItem(IDC_BUTTON6)->EnableWindow(false);
  //GetDlgItem(IDC_BUTTON6)->SetWindowTextA("服务器连接中...");

}

//-------------------------------------------------------------------------





void Cmo1Dlg::alltime(void)
{
  SYSTEMTIME sys;
  GetLocalTime(&sys);
  everytime.Format("%d /%d /%d---%d:%d:%d---星期", sys.wYear, sys.wMonth,
    sys.wDay, sys.wHour, sys.wMinute, sys.wSecond);
  nevertime.Format("%d/%d-%d:%d:%d", sys.wMonth, sys.wDay, sys.wHour,
    sys.wMinute, sys.wSecond);
  switch (sys.wDayOfWeek)
  {
    case 1:
      everytime = everytime + "一";
      break;
    case 2:
      everytime = everytime + "二";
      break;
    case 3:
      everytime = everytime + "三";
      break;
    case 4:
      everytime = everytime + "四";
      break;
    case 5:
      everytime = everytime + "五";
      break;
    case 6:
      everytime = everytime + "六";
      break;
    case 0:
      everytime = everytime + "日";
      break;

  }
}

//-------------------------------------------------------------------------


void Cmo1Dlg::OnBnClickedOk()
{
  // TODO: 在此添加控件通知处理程序代码
  exit(1);
  //CDialogEx::OnOK();
}

//-------------------------------------------------------------------------








void Cmo1Dlg::SavebmpFile(int dpi)
{
  int binbarwid;
  if (dpi == 0)
  {
    binbarwid = 3;
  }
  else
  {
    binbarwid = 4;
  }

  int bmpFilehigh = 21.64 *(binbarwid * 3);
  int bmpFilewid = strlen(BinBarText) * binbarwid;
  long lWidthBytes = (bmpFilewid * 24+31) / 32 * 4; //校正为4倍数

  BYTE * bmpdata = new BYTE[bmpFilehigh * lWidthBytes];
  memset(bmpdata, bmpFilehigh * lWidthBytes, 0);
  BinBarTobmpData(binbarwid, lWidthBytes, bmpFilehigh, BinBarText, bmpdata);
  //生成图像的信息头（包括位图文件头和位图信息）
  //位图文件头占14字节，位图信息头占40字节
  //真彩色位图没有调色板，所以图像的偏移就为54字节
  CString m_fileName;
  BITMAPFILEHEADER bf; //位图文件头
  bf.bfOffBits = 54;
  bf.bfReserved1 = 0;
  bf.bfReserved2 = 0;
  //说明文件大小（位图文件头+位图信息头+图片像素所占字节数）
  bf.bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + bmpFilehigh
    * bmpFilewid * 3;
  //说明文件类型为BM
  bf.bfType = ((WORD)'M' << 8 | 'B');
  BITMAPINFOHEADER bi; //位图信息头
  //说明每个像素所占字节数，其值为1、4、8、16、24、32
  bi.biBitCount = 24;
  //说明图像显示有重要影响的颜色索引的数目，如果是0，表示都重要
  bi.biClrImportant = 0;
  //说明位图实际使用的彩色表中的颜色索引数（设为0的话，则说明使用所有调色板项）
  bi.biClrUsed = 0;
  //说明图像数据压缩的类型
  bi.biCompression = 0L;
  //说明图像的宽度，以像素为单位
  bi.biWidth = bmpFilewid;
  //说明图像的高度，以像素为单位
  bi.biHeight = bmpFilehigh;
  //目标设备说明位面数，其值总是被设为1
  bi.biPlanes = 1;
  //说明BITMAPINFOHEADER结构所需要的字节数
  bi.biSize = sizeof(BITMAPINFOHEADER);
  //说明图像的大小，以字节为单位。当用BI_RGB格式时，可设置为0
  bi.biSizeImage = BI_RGB;
  //说明图像的水平分辨率，用像素/米来表示
  bi.biXPelsPerMeter = 0;
  //说明图像的垂直分辨率，用像素/米来表示
  bi.biYPelsPerMeter = 0;
  //打开保存对话框，获取保存路径及文件名
  CString filter;
  filter = "位图(*.bmp)|*.bmp|All Files(*.*)|*.*||";

  CFileDialog dlg(FALSE, "bmp", "UnName", OFN_HIDEREADONLY |
    OFN_OVERWRITEPROMPT, filter);
  //为TRUE则显示打开对话框，为FALSE则显示保存对话文件对话框。
  dlg.m_ofn.lpstrTitle = "别拖拖拉拉的，快保存";
  if (dlg.DoModal() == IDOK)
  {
    UpdateData(true);
    savename = m_fileName = dlg.GetPathName();
  }
  else
  {
    return ;
  }
  //建立CFile对象准备保存图片
  CFile file;
  if (!file.Open(m_fileName, CFile::modeCreate | CFile::modeWrite | CFile
    ::typeBinary))
  {
    AfxMessageBox("Can not write BMP file.");
    return ;
  }
  //写入位图文件头
  file.Write(&bf, 14);
  //写入位图信息头
  file.Write(&bi, sizeof(BITMAPINFOHEADER));
  //获得图像每一行所占的字节数
  //long lWidthBytes=(m_nWidth*m_nBitCount+31)/32*4;
  //写入整个的像素矩阵中的数据
  file.Write(bmpdata, bmpFilehigh * lWidthBytes);
  //写入完毕，关闭文件
  file.Close();
  delete []bmpdata;
}

//-------------------------------------------------------------------------

void Cmo1Dlg::BinBarTobmpData(int binbarwid, int lWidthBytes, int barbmphigh,
  char * binbar, BYTE * bmpdata)
{
  int binbarlen = strlen(binbar);
  int interhigh = 19.88 * binbarwid * 3;
  int barhigh = 18.23 * binbarwid * 3;

  int gray = 0;
  int high;
  int i;
  for (i = 0; i < binbarlen; i++)
  {
    high = 21.64 * binbarwid * 3;
    for (int j = 0; j < binbarwid; j++)
    {
      for (int k = 0; k < high; k++)
      {
        bmpdata[(barbmphigh - k - 1) * lWidthBytes + (i * binbarwid + j) * 3] =
          255; //b
        bmpdata[(barbmphigh - k - 1) * lWidthBytes + (i * binbarwid + j) * 3+1]
          = 255; //g
        bmpdata[(barbmphigh - k - 1) * lWidthBytes + (i * binbarwid + j) * 3+2]
          = 255; //r

      }
    }
  }
  for (i = 0; i < binbarlen; i++)
  {
    if (binbar[i] == '1')
    {
      high = barhigh;
      if (i == 7 || i == 9 || i == 39 || i == 41 || i == 71 || i == 73)
      {
        high = interhigh;
      }
      for (int j = 0; j < binbarwid; j++)
      {
        for (int k = 0; k < high; k++)
        {
          bmpdata[(barbmphigh - k - 1) * lWidthBytes + (i * binbarwid + j) * 3]
            = 0; //b
          bmpdata[(barbmphigh - k - 1) * lWidthBytes + (i * binbarwid + j) * 3
            + 1] = 0; //g
          bmpdata[(barbmphigh - k - 1) * lWidthBytes + (i * binbarwid + j) * 3
            + 2] = 0; //r
        }
      }
    }
  }
}

//-------------------------------------------------------------------------


void RGBToGray(unsigned char * image, int len, unsigned char * gray, int wid,
  int hi)
{
  int w = len / hi;
  for (int i = 0; i < hi; i++)
  for (int j = 0; j < wid; j++)
  {
    gray[i * wid + j] = unsigned char(0.11 * image[(hi - 1-i) * w + 3 * j] +
      0.59 * image[(hi - 1-i) * w + 3 * j + 1] + 0.30 * image[(hi - 1-i) * w +
      3 * j + 2]);
  }
}

//-------------------------------------------------------------------------

unsigned char * Load(const char * fileName, LONG &wid, LONG &hi)
{
  BITMAPFILEHEADER BFH; //位图文件头 关于文件类型、文件大小、存放位置等信息
  BITMAPINFOHEADER * pBIH; //位图信息头
  char * tmp = NULL;
  unsigned char * pImage = NULL;

  CFile cf;

  if (!cf.Open(fileName, CFile::modeRead))
  {
    return (NULL);
  }

  DWORD dwDibSize;

  dwDibSize = cf.GetLength() - sizeof(BITMAPFILEHEADER);
  //获取文件长  减去文件头

  tmp = new char[dwDibSize];
  if (tmp == NULL)
  {
    return (NULL);
  }

  if (cf.Read(&BFH, sizeof(BITMAPFILEHEADER)) != sizeof(BITMAPFILEHEADER)
  //读取文件头信息
   || BFH.bfType != 'MB' || cf.Read(tmp, dwDibSize) != dwDibSize)
  //判断文件类型和读文件内容
  {
    delete []tmp;
    return (NULL);
  }
  pBIH = (BITMAPINFOHEADER*)tmp; //位图信息头
  if (pBIH->biBitCount == 24)
  //判断是否为24位真彩图片
  {
    wid = pBIH->biWidth; //获取位图宽度
    hi = pBIH->biHeight; //获取位图高度
    LONG nWidModi = wid * 3;
    nWidModi += (4-nWidModi % 4) % 4; //校正图像宽度的字节数  是4的倍数

    pImage = new unsigned char[nWidModi * hi];
    if (pImage == NULL)
    {
      return NULL;
    }

    memcpy(pImage, &tmp[sizeof(BITMAPINFOHEADER)], dwDibSize - sizeof
      (BITMAPINFOHEADER)); //位图数据
  } /*else if((pBIH->biBitCount==8))  //判断是否为8位灰度图片
  {
  AfxMessageBox("The file is a 8_bits bitmap!");
  wid=pBIH->biWidth;
  hi=pBIH->biHeight ;
  wid+=(4-wid%4)%4;//校正图像宽度的字节数
  pImage=new unsigned char [3*wid*hi];
  if(pImage==NULL)
  return NULL;

  char* tmp1=&tmp[sizeof(BITMAPINFOHEADER)+4*256];   //位图数据指针

  for(LONG y=0;y<hi;y++)
  for(LONG x=0;x<wid;x++)
  {  //使用校正后的字节宽度值
  pImage[(y*wid+x)*3]=tmp1[y*wid+x];
  pImage[(y*wid+x)*3+1]=tmp1[y*wid+x];
  pImage[(y*wid+x)*3+2]=tmp1[y*wid+x];
  }
  }else if((pBIH->biBitCount==1))  //判断是否为2位灰度图片
  {
  AfxMessageBox("The file is a 2_bits bitmap!");
  wid=pBIH->biWidth;
  hi=pBIH->biHeight ;
  wid+=(32-wid%32)%32;//校正图像宽度的字节数

  pImage=new unsigned char [3*wid*hi];
  if(pImage==NULL)
  return NULL;
  char* tmp1=&tmp[sizeof(BITMAPINFOHEADER)+4];   //位图数据指针
  for(LONG y=0;y<hi;y++)
  for(LONG x=0;x<wid/8;x++)
  {  //使用校正后的字节宽度值
  unsigned char b=128;
  char data=tmp1[y*wid/8+x];;
  for(int i=0;i<8;i++){
  if(b&data){
  pImage[(y*wid+x*8+i)*3]=255;
  pImage[(y*wid+x*8+i)*3+1]=255;
  pImage[(y*wid+x*8+i)*3+2]=255;
  }
  else{
  pImage[(y*wid+x*8+i)*3]=0;
  pImage[(y*wid+x*8+i)*3+1]=0;
  pImage[(y*wid+x*8+i)*3+2]=0;
  }
  b>>=1;
  //	 CString str;
  //   str.Format(" %d %d",b,x);
  //  AfxMessageBox(str);
  }			
  }
  } */
  delete tmp;
  cf.Close();
  return pImage;
}

//-------------------------------------------------------------------------

void BiGray(unsigned char * gray, int wid, int hi)
{
  int i, j;
  int g1, g2;


  unsigned char * p;
  unsigned char thresh;
  int hist[256];
  unsigned char iMaxGray, iMinGray;
  unsigned char NewThresh;
  long int LP1, LP2, LS1, LS2;
  unsigned char imeanGray1, imeanGray2;
  int times;
  iMaxGray = 0;
  iMinGray = 0;

  for (i = 0; i < 256; i++)
  {
    hist[i] = 0;
  }

  j = wid * hi;
  p = gray;
  for (i = 0; i < j; i++)
  {
    hist[ * p++]++;
  }

  //得到最大与最小的灰度级
  j = 0;
  i = 0;
  while (j < int(0.0001 * wid * hi))
  {
    j += hist[i];
    i++;
  }
  iMinGray = g1 = i - 1;

  i = 255;
  j = 0;
  while (j < int(0.0001 * wid * hi))
  {
    j += hist[i];
    i--;
  }
  iMaxGray = g2 = i + 1;


  NewThresh = (iMinGray + iMaxGray) / 2;
  thresh = 0;
  for (times = 0; ((NewThresh - thresh) > 1 || (NewThresh - thresh) <  - 1);
    times++)
  {
    thresh = NewThresh;
    LP1 = 0;
    LP2 = 0;
    LS1 = 0;
    LS2 = 0;
    for (i = iMinGray; i < thresh; i++)
    {
      LP1 += hist[i] * i;
      LS1 += hist[i];
    }

    imeanGray1 = (unsigned char)(LP1 / LS1);

    for (i = thresh + 1; i <= iMaxGray; i++)
    {
      LP2 += hist[i] * i;
      LS2 += hist[i];
    }
    imeanGray2 = (unsigned char)(LP2 / LS2);
    NewThresh = (imeanGray1 + imeanGray2) / 2;
  }

  unsigned char * gray1 = new unsigned char[wid * hi];

  for (i = 0; i < wid * hi; i++)
  if (gray[i] < NewThresh)
  {
    gray[i] = 0;
  }
  else
  {
    gray[i] = 255;
  }

}

//-------------------------------------------------------------------------

BOOL SaveBmp(HBITMAP hBitmap, CString FileName)
{

  if (hBitmap == NULL || FileName.IsEmpty())
  {
    //if (hBitmap==NULL)
    //	AfxMessageBox("参数错误2");

    return false;
  }


  HDC hDC;
  //当前分辨率下每象素所占字节数
  int iBits;
  //位图中每象素所占字节数
  WORD wBitCount;
  //定义调色板大小， 位图中像素字节大小 ，位图文件大小 ， 写入文件字节数
  DWORD dwPaletteSize = 0, dwBmBitsSize = 0, dwDIBSize = 0, dwWritten = 0;
  //位图属性结构
  BITMAP Bitmap;
  //位图文件头结构
  BITMAPFILEHEADER bmfHdr;
  //位图信息头结构
  BITMAPINFOHEADER bi;
  //指向位图信息头结构
  LPBITMAPINFOHEADER lpbi;
  //定义文件，分配内存句柄，调色板句柄
  HANDLE fh, hDib, hPal, hOldPal = NULL;

  //计算位图文件每个像素所占字节数
  hDC = CreateDC("DISPLAY", NULL, NULL, NULL);
  iBits = GetDeviceCaps(hDC, BITSPIXEL) * GetDeviceCaps(hDC, PLANES);
  DeleteDC(hDC);
  if (iBits <= 1)
  {
    wBitCount = 1;
  }
  else if (iBits <= 4)
  {
    wBitCount = 4;
  }
  else if (iBits <= 8)
  {
    wBitCount = 8;
  }
  else
  {
    wBitCount = 24;
  }


  GetObject(hBitmap, sizeof(Bitmap), (LPSTR) &Bitmap);
  bi.biSize = sizeof(BITMAPINFOHEADER);
  bi.biWidth = Bitmap.bmWidth;
  bi.biHeight = Bitmap.bmHeight;
  bi.biPlanes = 1; //总设为1
  bi.biBitCount = wBitCount;
  bi.biCompression = BI_RGB; //不压缩
  bi.biSizeImage = 0; //使用BI_RGB，设为0
  bi.biXPelsPerMeter = 0;
  bi.biYPelsPerMeter = 0;
  bi.biClrImportant = 0; //（设为0的话，则说明使用所有调色板项）
  bi.biClrUsed = 0;
  //说明对图象显示有重要影响的颜色索引的数目，如果是0，表示都重要。

  dwBmBitsSize = ((Bitmap.bmWidth * wBitCount + 31) / 32) * 4 * Bitmap.bmHeight;

  //为位图内容分配内存
  hDib = GlobalAlloc(GHND, dwBmBitsSize + dwPaletteSize + sizeof
    (BITMAPINFOHEADER));
  lpbi = (LPBITMAPINFOHEADER)GlobalLock(hDib);
  *lpbi = bi;

  // 处理调色板
  hPal = GetStockObject(DEFAULT_PALETTE);
  if (hPal)
  {
    hDC = ::GetDC(NULL);
    hOldPal = ::SelectPalette(hDC, (HPALETTE)hPal, FALSE);
    RealizePalette(hDC);
  }

  // 获取该调色板下新的像素值
  GetDIBits(hDC, hBitmap, 0, (UINT)Bitmap.bmHeight, (LPSTR)lpbi + sizeof
    (BITMAPINFOHEADER) + dwPaletteSize, (BITMAPINFO*)lpbi, DIB_RGB_COLORS);

  //恢复调色板
  if (hOldPal)
  {
    ::SelectPalette(hDC, (HPALETTE)hOldPal, TRUE);
    RealizePalette(hDC);
    ::ReleaseDC(NULL, hDC);
  }

  //创建位图文件
  fh = CreateFile(FileName, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
    FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

  if (fh == INVALID_HANDLE_VALUE)
  {
    return FALSE;
  }

  // 设置位图文件头
  bmfHdr.bfType = 0x4D42; // "BM"
  dwDIBSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) +
    dwPaletteSize + dwBmBitsSize;
  bmfHdr.bfSize = dwDIBSize;
  bmfHdr.bfReserved1 = 0;
  bmfHdr.bfReserved2 = 0;
  bmfHdr.bfOffBits = (DWORD)sizeof(BITMAPFILEHEADER) + (DWORD)sizeof
    (BITMAPINFOHEADER) + dwPaletteSize;

  // 写入位图文件头
  WriteFile(fh, (LPSTR) &bmfHdr, sizeof(BITMAPFILEHEADER), &dwWritten, NULL);

  // 写入位图文件其余内容
  WriteFile(fh, (LPSTR)lpbi, dwDIBSize - sizeof(BITMAPFILEHEADER), &dwWritten,
    NULL);
  //清除
  GlobalUnlock(hDib);
  GlobalFree(hDib);
  CloseHandle(fh);

  return TRUE;
}

//-------------------------------------------------------------------------

void GetBBlenLink(BYTE * pGray, int wid, int hi, struct binlen *  &bl)
{
  if (pGray == NULL)
  {
    return ;
  }

  struct binlen * p,  * q;
  p = new struct binlen;
  p->next = NULL;
  bl = p;

  int hpos = hi / 2;
  int flag;
  flag = pGray[hpos * wid];
  int cnt;
  cnt = 1;
  for (int i = 1; i < wid; i++)
  {
    while (i < wid && flag == pGray[hpos * wid + i])
    {
      cnt++;
      i++;
    } p->flag = flag;
    p->cnt = cnt;
    q = new struct binlen;
    q->next = NULL;
    p->next = q;
    p = q;
    flag = pGray[hpos * wid + i];
    cnt = 1;
  }
}

//-------------------------------------------------------------------------

void GetBinCode(struct binlen * bl, char * bincode)
{
  int flag0 = 0;
  int flag255 = 255;
  int bincodelen;
  struct binlen * p;
  p = bl;
  while (p != NULL && p->flag == flag255)
  {
    p = p->next;
  }
  //跳过空白区域，寻找起始条纹
  bincodelen = p->cnt; //起始条纹宽度为单一条纹宽度

  int bincnt;
  int i = 0;
  while (p != NULL && (p->flag == flag0 || p->flag == flag255))
  {
    bincnt = p->cnt / bincodelen;
    if (p->flag == flag0)
    {
      for (int j = 0; j < bincnt; j++)
      {
        bincode[i] = '1';
        i++;
      }
    }
    if (p->flag == flag255)
    {
      for (int j = 0; j < bincnt; j++)
      {
        bincode[i] = '0';
        i++;
      }
    }
    p = p->next;
  }
  bincode[i] = '\0';
}

//-------------------------------------------------------------------------

void GetBarText(char st[][3][8], char * binbar, char * bartext)
{
  int i = 0; //二进制编码序号
  int j = 0; //条码文字序号
  int l;
  int k;
  char tmp[8];
  tmp[7] = '\0';
  i += 3; //跳过起始码101
  for (k = 0; k < 4; k++)
  {
    strncpy(tmp, &binbar[i], 7);
    i += 7;
    for (l = 0; l < 10; l++)
    if (strcmp(tmp, st[l][0]) == 0)
    {
      bartext[j] = l + '0';
      j++;
      break;
    }
  }

  i += 5;

  for (k = 0; k < 4; k++)
  {
    strncpy(tmp, &binbar[i], 7);
    i += 7;
    for (l = 0; l < 10; l++)
    if (strcmp(tmp, st[l][2]) == 0)
    {
      bartext[j] = l + '0';
      j++;
      break;
    }
  }
  bartext[j] = '\0';
}


void Cmo1Dlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	
	/*AfxMessageBox("as");
	CRect rect;
	GetDlgItem(IDC_BUTTON1)->GetWindowRect(&rect);
	//ScreenToClient(rect);
	CString str;
	str.Format("上%d    下%d    左%d    右%d    X%d    Y%d",rect.top,rect.bottom,rect.left,rect.right,point.x,point.y);
	UpdateWindow();
	GetDlgItem(IDC_EDIT1)->SetWindowText(str);
	UpdateWindow();
	if((rect.top>point.y)&&(rect.bottom<point.y)&&(rect.left<point.x)&&(rect.right>point.x))
		AfxMessageBox("as");
	/*
	CRect rect;
	GetDlgItem(IDC_LIST1)->GetWindowRect(&rect);
	ScreenToClient(rect); //转换相对于对话框的坐标
	CBrush brush(RGB(255, 255, 255));
	pDC->FillRect(&rect, &brush);
	pDC->SetStretchBltMode(COLORONCOLOR); //删除所有消除的像素行
	*/


	CDialogEx::OnMouseMove(nFlags, point);
}




void Cmo1Dlg::setbuttoncoltor(void)    //白按钮
{
	CBitmap Bitmap;
	Bitmap.LoadBitmap(IDB_BITMAP2);
	HBITMAP hBitmap = (HBITMAP)Bitmap.Detach();
	CButton * pButton = (CButton*)GetDlgItem(IDOK);
	pButton->SetBitmap(hBitmap);

	
	CBitmap Bitmap1;
	Bitmap1.LoadBitmap(IDB_BITMAP6);
	HBITMAP hBitmap1 = (HBITMAP)Bitmap1.Detach();
	CButton * pButton1 = (CButton*)GetDlgItem(IDC_BUTTON1);
	pButton1->SetBitmap(hBitmap1);

	CBitmap Bitmap2;
	Bitmap2.LoadBitmap(IDB_BITMAP7);
	HBITMAP hBitmap2 = (HBITMAP)Bitmap2.Detach();
	CButton * pButton2 = (CButton*)GetDlgItem(IDC_BUTTON2);
	pButton2->SetBitmap(hBitmap2);

	CBitmap Bitmap3;
	Bitmap3.LoadBitmap(IDB_BITMAP4);
	HBITMAP hBitmap3 = (HBITMAP)Bitmap3.Detach();
	CButton * pButton3 = (CButton*)GetDlgItem(IDC_BUTTON3);
	pButton3->SetBitmap(hBitmap3);

	CBitmap Bitmap4;
	Bitmap4.LoadBitmap(IDB_BITMAP8);
	HBITMAP hBitmap4 = (HBITMAP)Bitmap4.Detach();
	CButton * pButton4 = (CButton*)GetDlgItem(IDC_BUTTON4);
	pButton4->SetBitmap(hBitmap4);

	CBitmap Bitmap5;
	Bitmap5.LoadBitmap(IDB_BITMAP5);
	HBITMAP hBitmap5 = (HBITMAP)Bitmap5.Detach();
	CButton * pButton5 = (CButton*)GetDlgItem(IDC_BUTTON5);
	pButton5->SetBitmap(hBitmap5);
	
}


void Cmo1Dlg::setbuttoncoltor2(void)    //青按钮
{
	CBitmap Bitmap;
	Bitmap.LoadBitmap(IDB_BITMAP10);
	HBITMAP hBitmap = (HBITMAP)Bitmap.Detach();
	CButton * pButton = (CButton*)GetDlgItem(IDOK);
	pButton->SetBitmap(hBitmap);
	/*
	CBitmap Bitmap1;
	Bitmap1.LoadBitmap(IDB_BITMAP11);
	HBITMAP hBitmap1 = (HBITMAP)Bitmap1.Detach();
	CButton * pButton1 = (CButton*)GetDlgItem(IDC_BUTTON1);
	pButton1->SetBitmap(hBitmap1);

	CBitmap Bitmap2;
	Bitmap2.LoadBitmap(IDB_BITMAP12);
	HBITMAP hBitmap2 = (HBITMAP)Bitmap2.Detach();
	CButton * pButton2 = (CButton*)GetDlgItem(IDC_BUTTON2);
	pButton2->SetBitmap(hBitmap2);

	CBitmap Bitmap3;
	Bitmap3.LoadBitmap(IDB_BITMAP13);
	HBITMAP hBitmap3 = (HBITMAP)Bitmap3.Detach();
	CButton * pButton3 = (CButton*)GetDlgItem(IDC_BUTTON3);
	pButton3->SetBitmap(hBitmap3);

	CBitmap Bitmap4;
	Bitmap4.LoadBitmap(IDB_BITMAP14);
	HBITMAP hBitmap4 = (HBITMAP)Bitmap4.Detach();
	CButton * pButton4 = (CButton*)GetDlgItem(IDC_BUTTON4);
	pButton4->SetBitmap(hBitmap4);

	CBitmap Bitmap5;
	Bitmap5.LoadBitmap(IDB_BITMAP15);
	HBITMAP hBitmap5 = (HBITMAP)Bitmap5.Detach();
	CButton * pButton5 = (CButton*)GetDlgItem(IDC_BUTTON5);
	pButton5->SetBitmap(hBitmap5);
	*/
}
